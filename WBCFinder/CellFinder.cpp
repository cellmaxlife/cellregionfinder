#include "stdafx.h"
#include "CellFinder.h"

#define MIN_ARTIFACT_PIXEL_COUNT 1000

template<typename T>
void CCellFinder::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CCellFinder::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CCellFinder::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CCellFinder::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CCellFinder::SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList)
{
	int length = PATTERN_WIDTH * PATTERN_HEIGHT;
	unsigned short *imgPtr = image;
	for (int i = 0; i < length; i++, imgPtr++)
	{
		int intensity = (int) *imgPtr;
		AddOnePixel(sortedList, *imgPtr, i);
	}
}

void CCellFinder::GetSortedPixelList(unsigned short *image, vector<CBlobData *> *blobList, vector<INTENSITY_PIXEL_SET *> *sortedList)
{
	SortPixelIntensities(image, sortedList);
	if (sortedList->size() > 0)
	{
		int maxCount = 0;
		int maxIndex = -1;
		for (int i = 0; i < (int)sortedList->size(); i++)
		{
			if ((int)(*sortedList)[i]->pixelList->size() > maxCount)
			{
				maxIndex = i;
				maxCount = (int)(*sortedList)[i]->pixelList->size();
			}
		}
		if (maxCount > MIN_ARTIFACT_PIXEL_COUNT)
		{
			delete (*sortedList)[maxIndex];
			(*sortedList)[maxIndex] = NULL;
			vector<INTENSITY_PIXEL_SET *>::iterator it0 = sortedList->begin() + maxIndex;
			sortedList->erase(it0);
		}
	}
}

void CCellFinder::AddOnePixel(vector<INTENSITY_PIXEL_SET *> *list, unsigned short intensity, int position)
{
	vector<INTENSITY_PIXEL_SET *>::iterator it0;
	for (it0 = list->begin(); it0 != list->end(); it0++)
	{
		if (intensity <= (*it0)->intensity)
			break;
	}
	if (it0 == list->end())
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->push_back(newSet);
	}
	else if ((*it0)->intensity == intensity)
	{
		(*it0)->pixelList->push_back(position);
	}
	else
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->insert(it0, newSet);
	}
}

void CCellFinder::ScanPixelsForBlobs(vector<INTENSITY_PIXEL_SET *> *sortedList, int maxIntensityIndex, vector<CBlobData *> *blobList)
{
	vector<int> *pixels = (*sortedList)[maxIntensityIndex]->pixelList;
	vector<int> remainingPixels;
	while (pixels->size() > 0)
	{
		CBlobData *blob = new CBlobData(PATTERN_WIDTH, PATTERN_HEIGHT);
		ScanPixelsForOneBlob(pixels, blob, &remainingPixels);
		if (blob->m_Pixels->size() > MIN_ARTIFACT_PIXEL_COUNT)
		{
			blobList->push_back(blob);
			ScanPixelsForBlobBoundary(blob->m_Pixels, blob->m_Artifact);
			blob->m_Pixels->clear();
			blob = NULL;
		}
		else if (blob->m_Pixels->size() > 0)
		{
			for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
				remainingPixels.push_back((*blob->m_Pixels)[i]);
		}
		if (blob != NULL)
		{
			delete blob;
			blob = NULL;
		}
	}
	if (remainingPixels.size() > 0)
	{
		QuickSort(&remainingPixels, 0, (int)remainingPixels.size() - 1);
		for (int i = 0; i < (int)remainingPixels.size(); i++)
		{
			pixels->push_back(remainingPixels[i]);
		}
		remainingPixels.clear();
	}
}

void CCellFinder::ScanPixelsForOneBlob(vector<int> *pixels, CBlobData *blob, vector<int> *remainingPixels)
{
	while (pixels->size() > 0)
	{
		bool ret = false;
		vector<int>::iterator it0 = pixels->begin();
		int position = (*it0);
		pixels->erase(it0);
		vector<int> *blobPixelPtr = blob->m_Pixels;
		if (blobPixelPtr->size() == 0)
		{
			if (ForwardScan(pixels, position + 1))
				blobPixelPtr->push_back(position);
			else
				remainingPixels->push_back(position);
		}
		else
		{
			int leftPos = position - 1;
			int topPos = position - PATTERN_WIDTH;
			if (((leftPos >= 0) && (blobPixelPtr->size() > 0) &&
				(leftPos == (*blobPixelPtr)[blobPixelPtr->size() - 1])) ||
				((topPos >= 0) && (BackwardScan(blobPixelPtr, topPos))))
			{
				ret = true;
			}
			if (!ret)
			{
				remainingPixels->push_back(position);
				break;
			}
			else
				blobPixelPtr->push_back(position);
		}
	}
}

bool CCellFinder::BackwardScan(vector<int> *pixels, int position)
{
	bool ret = false;
	for (int i=(int)pixels->size() - 1; i>=0; i--)
	{
		if ((*pixels)[i] == position)
		{
			ret = true;
			break;
		}
		else if ((*pixels)[i] < position)
		{
			break;
		}
	}

	return ret;
}

bool CCellFinder::ForwardScan(vector<int> *pixels, int position)
{
	bool ret = false;
	for (int i = 0; i <(int)pixels->size(); i++)
	{
		if ((*pixels)[i] == position)
		{
			ret = true;
			break;
		}
		else if ((*pixels)[i] > position)
		{
			break;
		}
	}

	return ret;
}

void CCellFinder::ScanPixelsForBlobBoundary(vector<int> *pixels, vector<int> *boundary)
{
	vector<int>::iterator it0 = pixels->begin();
	while (it0 != pixels->end())
	{
		int position = (*it0);
		int leftPos = position - 1;
		int topPos = position - PATTERN_WIDTH;
		int rightPos = position + 1;
		int bottomPos = position + PATTERN_WIDTH;
		int maxPos = PATTERN_WIDTH * PATTERN_HEIGHT;
		if (!(((leftPos >= 0) && (BackwardScan(pixels, leftPos))) &&
			((topPos >= 0) && (BackwardScan(pixels, topPos))) &&
			((rightPos < maxPos) && (ForwardScan(pixels, rightPos))) &&
			((bottomPos < maxPos) && (ForwardScan(pixels, bottomPos)))))
		{
			boundary->push_back(position);
		}
		else if (((position % PATTERN_WIDTH) == 0) ||
			((position % PATTERN_WIDTH) == (PATTERN_WIDTH - 1)) ||
			((position >= 0) && (position < PATTERN_WIDTH)) ||
			((position >= (PATTERN_WIDTH * (PATTERN_HEIGHT - 1))) &&
				(position < (PATTERN_WIDTH * PATTERN_HEIGHT))))
		{
			boundary->push_back(position);
		}
		it0++;
	}
}