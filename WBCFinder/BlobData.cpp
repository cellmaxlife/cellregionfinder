#include "stdafx.h"
#include "BlobData.h"

CBlobData::CBlobData(int width, int height)
{
	m_Width = width;
	m_Height = height;
	m_Boundary = new vector<int>();
	m_Pixels = new vector<int>();
	m_Artifact = new vector<int>();
	m_Intensity = 0;
	m_IntensityPos = 0;
	m_Threshold = 0;
}

CBlobData::~CBlobData()
{
	m_Boundary->clear();
	delete m_Boundary;
	m_Pixels->clear();
	delete m_Pixels;
	m_Artifact->clear();
	delete m_Artifact;
}

