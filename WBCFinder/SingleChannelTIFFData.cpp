#include "stdafx.h"
#include "SingleChannelTIFFData.h"

#define SUBSAMPLERATE	32

#define NUM_TAGS_IN_LEICA_TIFF 13
#define NUM_TAGS_IN_ZEISS_TIFF 21
#define NUM_BYTES_IN_ONE_TAG   12

const int TIFFTAG_IMAGEWIDTH = 256;	/* image width in pixels */
const int TIFFTAG_IMAGELENGTH = 257;	/* image height in pixels */
const int TIFFTAG_BITSPERSAMPLE = 258;	/* bits per channel (sample) */
const int TIFFTAG_COMPRESSION = 259;	/* data compression technique */
const int COMPRESSION_NONE = 1;	/* dump mode */
const int COMPRESSION_CCITTRLE = 2;	/* CCITT modified Huffman RLE */
const int COMPRESSION_CCITTFAX3 = 3;	/* CCITT Group 3 fax encoding */
const int COMPRESSION_CCITTFAX4 = 4;	/* CCITT Group 4 fax encoding */
const int COMPRESSION_LZW = 5;	/* Lempel-Ziv  & Welch */
const int COMPRESSION_JPEG = 6;	/* !JPEG compression */
const int TIFFTAG_PHOTOMETRIC = 262;	/* photometric interpretation */
const int PHOTOMETRIC_MINISWHITE = 0;	/* min value is white */
const int PHOTOMETRIC_MINISBLACK = 1;	/* min value is black */
const int PHOTOMETRIC_RGB = 2;	/* RGB color model */
const int PHOTOMETRIC_PALETTE = 3;	/* color map indexed */
const int PHOTOMETRIC_MASK = 4;	/* $holdout mask */
const int PHOTOMETRIC_SEPARATED = 5;	/* !color separations */
const int PHOTOMETRIC_YCBCR = 6;	/* !CCIR 601 */
const int PHOTOMETRIC_CIELAB = 8;	/* !1976 CIE L*a*b* */
const int TIFFTAG_STRIPOFFSETS = 273;	/* offsets to data strips */
const int TIFFTAG_SAMPLESPERPIXEL = 277;	/* samples per pixel */
const int TIFFTAG_ROWSPERSTRIP = 278;	/* rows per strip of data */
const int TIFFTAG_STRIPBYTECOUNTS = 279;	/* bytes counts for strips */

CSingleChannelTIFFData::CSingleChannelTIFFData()
{
	m_Width = 0;
	m_Height = 0;
	m_Image = NULL;
	m_CPI = 0;
}

CSingleChannelTIFFData::~CSingleChannelTIFFData(void)
{
	FreeImage();
}

void CSingleChannelTIFFData::FreeImage()
{
	if (m_Image != NULL)
	{
		for (int i = 0; i < m_Height; i++)
		{
			delete m_Image[i];
			m_Image[i] = NULL;
		}

		delete m_Image;
		m_Image = NULL;
	}
}

bool CSingleChannelTIFFData::LoadRawTIFFFile(CString filename)
{
	bool status = false;
	m_ZeissData = false;
	FreeImage();
	CFile theFile;
	UINT DefaultReadSize = (UINT)4096;
	CString message;

	if (theFile.Open(filename, CFile::modeRead))
	{
		UINT fileSize = (UINT)theFile.GetLength();
		BYTE *buf = new BYTE[fileSize];
		BYTE *buf1 = buf;
		UINT sizeRead = 0;
		UINT sizeToRead = (UINT) DefaultReadSize;
		UINT sizeRemain = fileSize;
		while (true)
		{
			if (sizeRemain > DefaultReadSize)
			{
				sizeRead = theFile.Read(buf1, sizeToRead);
				if (sizeRead != sizeToRead)
				{
					break;
				}
				else
				{
					buf1 += sizeRead;
					sizeRemain -= DefaultReadSize;
				}
			}
			else if (sizeRemain > (UINT)0)
			{
				sizeToRead = sizeRemain;
				sizeRead = theFile.Read(buf1, sizeToRead);
				if (sizeRead != sizeToRead)
				{
					break;
				}
				else
				{
					buf1 += sizeRead;
					sizeRemain -= (ULONGLONG)sizeRead;
				}
			}
			else
			{
				status = DecodeTIFFBufData(buf, fileSize);
				break;
			}
		}
		delete buf;
		theFile.Close();
	}

	return status;
}

bool CSingleChannelTIFFData::DecodeTIFFBufData(BYTE *buf, ULONGLONG bufSize)
{
	bool ret = false;
	CString message;
	UINT32 bufLength = (UINT32)bufSize;
	UINT32 *StripOffsetArray = NULL;
	UINT32 *StripByteCountArray = NULL;
	UINT32 *hist = NULL;

	while (true)
	{
		if (bufSize < (ULONGLONG)8)
		{
			break;
		}
		if ((buf[0] != buf[1]) || (buf[0] != 'I'))
		{
			break;
		}
		UINT32 bufIndex;
		bufIndex = *((UINT32*)&buf[4]);
		if (bufIndex >= bufLength)
		{
			break;
		}
		int len = (int)bufLength - (int)bufIndex;
		UINT16 numTags = *((UINT16*)&buf[bufIndex]);
		if (numTags >= NUM_TAGS_IN_ZEISS_TIFF)
		{
			m_ZeissData = true;
		}

		if ((bufIndex + (numTags * NUM_BYTES_IN_ONE_TAG)) > bufLength)
		{
			break;
		}
		bufIndex += 2;
		UINT16 tag;
		BOOL exitWhileLoop = FALSE;
		UINT32 CountForStripOffsetArray = 0;
		UINT32 OffsetToStripOffsetArray = 0;
		UINT32 CountForStripByteCountArray = 0;
		UINT32 OffsetToStripByteCountArray = 0;
		for (int i = 0; i < numTags; i++, bufIndex += NUM_BYTES_IN_ONE_TAG)
		{
			tag = *((UINT16 *)&buf[bufIndex]);
			if (tag == TIFFTAG_IMAGEWIDTH)
			{
				m_Width = *((UINT32 *)&buf[bufIndex + 8]);
			}
			else if (tag == TIFFTAG_IMAGELENGTH)
			{
				m_Height = *((UINT32 *)&buf[bufIndex + 8]);
			}
			else if (tag == TIFFTAG_BITSPERSAMPLE)
			{
				UINT32 BitsPerPixel = *((UINT32 *)&buf[bufIndex + 8]);
				if (BitsPerPixel != 16)
				{
					exitWhileLoop = TRUE;
					break;
				}
			}
			else if (tag == TIFFTAG_STRIPOFFSETS)
			{
				CountForStripOffsetArray = *((UINT32 *)&buf[bufIndex + 4]);
				OffsetToStripOffsetArray = *((UINT32 *)&buf[bufIndex + 8]);
			}
			else if (tag == TIFFTAG_STRIPBYTECOUNTS)
			{
				CountForStripByteCountArray = *((UINT32 *)&buf[bufIndex + 4]);
				OffsetToStripByteCountArray = *((UINT32 *)&buf[bufIndex + 8]);
			}
		}

		if (exitWhileLoop)
			break;

		if (CountForStripOffsetArray > 0)
			StripOffsetArray = new UINT32[CountForStripOffsetArray];
		else
		{
			break;
		}

		bufIndex = OffsetToStripOffsetArray;
		if (bufIndex >= bufLength)
		{
			break;
		}
		len = (int)(4 * CountForStripOffsetArray);
		if ((bufIndex + len) > bufLength)
		{
			break;
		}

		for (int j = 0; j < (int)CountForStripOffsetArray; j++, bufIndex += 4)
		{
			StripOffsetArray[j] = *((UINT32 *)&buf[bufIndex]);
		}

		if (CountForStripByteCountArray > 0)
			StripByteCountArray = new UINT32[CountForStripByteCountArray];
		else
		{
			break;
		}

		bufIndex = OffsetToStripByteCountArray;
		if (bufIndex >= bufLength)
		{
			break;
		}
		len = (int)(4 * CountForStripByteCountArray);
		if ((bufIndex + len) > bufLength)
		{
			break;
		}

		for (int j = 0; j < (int)CountForStripByteCountArray; j++, bufIndex += 4)
		{
			StripByteCountArray[j] = *((UINT32 *)&buf[bufIndex]);
		}

		if ((m_Width == 0) || (m_Height == 0))
		{
			break;
		}
				
		m_Image = new unsigned short*[m_Height];
		for (int i = 0; i < m_Height; i++)
			m_Image[i] = new unsigned short[m_Width];

		hist = new UINT32[CPI_RANGE_LENGTH];
		memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);

		int rowIndex = 0;
		int columnIndex = 0;
		int numPixels;
		for (int i = 0; i < (int)CountForStripOffsetArray; i++)
		{
			bufIndex = StripOffsetArray[i];	
			if (bufIndex >= bufLength)
			{
				exitWhileLoop = true;
				break;
			}
			len = (int)StripByteCountArray[i];
			if ((bufIndex + len) > bufLength)
			{
				exitWhileLoop = true;
				break;
			}

			numPixels = (int)(len / 2);
			for (int j = 0; j < numPixels; j++, bufIndex += 2)
			{
				unsigned short value = *((unsigned short *)&buf[bufIndex]);
				if (m_ZeissData)
					value /= 4;
				if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
					hist[value-CPI_RANGE_START]++;
				m_Image[rowIndex][columnIndex] = value;
				columnIndex++;
				if (columnIndex == m_Width)
				{
					columnIndex = 0;
					rowIndex++;
				}
			}
		}
		
		if ((columnIndex == 0) && (rowIndex == m_Height))
		{
			UINT32 maxHistCount = 0;
			int maxHistIndex = 0;
			for (int i = 0; i < CPI_RANGE_LENGTH; i++)
			{
				if (hist[i] > maxHistCount)
				{
					maxHistCount = hist[i];
					maxHistIndex = i;
				}
			}
			m_CPI = maxHistIndex + CPI_RANGE_START;
		}
		else
		{
			exitWhileLoop = true;
		}

		delete[] hist;
		hist = NULL;

		if (StripByteCountArray != NULL)
		{
			delete StripByteCountArray;
			StripByteCountArray = NULL;
		}

		if (StripOffsetArray != NULL)
		{
			delete StripOffsetArray;
			StripOffsetArray = NULL;
		}

		if (exitWhileLoop)
		{
			if (m_Image != NULL)
			{
				for (int i = 0; i < m_Height; i++)
				{
					if (m_Image[i] != NULL)
					{
						delete m_Image[i];
						m_Image[i] = NULL;
					}
				}

				delete m_Image;
				m_Image = NULL;
			}
			break;
		}

		ret = true;
		break;
	}
	
	return ret;
}


int CSingleChannelTIFFData::GetImageWidth(void)
{
	return m_Width;
}

int CSingleChannelTIFFData::GetImageHeight(void)
{
	return m_Height;
}

bool CSingleChannelTIFFData::GetImageRegion(int x0, int y0, int width, int height, unsigned short *image)
{
	bool ret = false;
	CString message;
	
	while (TRUE)
	{
		if ((x0 < 0) || (y0 < 0) || ((x0 + width) > m_Width) || ((y0 + height) > m_Height))
		{
			break;
		}

		if (image == NULL)
		{
			break;
		}

		if (m_Image == NULL)
		{
			break;
		}

		for (int i = 0; i < height; i++)
		{
			memcpy(&image[width * i], &(m_Image[y0+i][x0]), sizeof(unsigned short) * width);
		}
		ret = true;
		break;
	}
	return ret;
}

bool CSingleChannelTIFFData::IsZeissData()
{
	return m_ZeissData;
}

unsigned short CSingleChannelTIFFData::GetCPI()
{
	return m_CPI;
}

int CSingleChannelTIFFData::GetSubsampledWidth()
{
	return (m_Width / SUBSAMPLERATE);
}

int CSingleChannelTIFFData::GetSubsampledHeight()
{
	return (m_Height / SUBSAMPLERATE);
}

bool CSingleChannelTIFFData::GetSubsampleImage(unsigned short *image)
{
	bool ret = true;
	unsigned short *ptr = image;
	int width = GetSubsampledWidth();
	int height = GetSubsampledHeight();

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptr++)
		{
			*ptr = m_Image[i * SUBSAMPLERATE][j * SUBSAMPLERATE];
		}
	}

	return ret;
}

void CSingleChannelTIFFData::CleanUpExclusionArea(int centerX, int centerY, int radiusX, int radiusY)
{
	int radius = radiusY;
	if (radiusX < radius)
		radius = radiusX;
	radius -= 10;
	int i0 = centerY - radius;
	int i1 = centerY + radius;
	for (int i = 0; i < m_Height; i++)
	{
		if ((i <= i0) || (i >= i1))
		{
			for (int j = 0; j < m_Width; j++)
				m_Image[i][j] = 0;
		}
		else 
		{
			int i2 = centerY - i;
			int j2 = radius * radius - i2 * i2;
			j2 = (int)sqrt((double)j2);
			for (int j = 0; j < (centerX - j2); j++)
				m_Image[i][j] = 0;
			for (int j = (centerX + j2); j < m_Width; j++)
				m_Image[i][j] = 0;
		}
 	}
}

