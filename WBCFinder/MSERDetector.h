#include "CTCParams.h"
#include "RGNData.h"
#include "RegionDataSet.h"
#include "ColorType.h"
#include "BlobData.h"
#include "Log.h"

#define SAMPLEFRAMEWIDTH 100
#define SAMPLEFRAMEHEIGHT 100
#define DEFAULT_MIN_BLOBPIXELS	20
#define BLOB_STACK_STEP	5

class CMSERDetector
{
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Partition(vector<int> *list, int lo, int hi);
	int ChoosePivot(vector<int> *list, int lo, int hi);
	int Partition(vector<INTENSITY_PIXEL_SET *> *list, int lo, int hi);
	int ChoosePivot(vector<INTENSITY_PIXEL_SET *> *list, int lo, int hi);
	int Find(int position, int *regionMap, int *regionLocation, int maxPosition);
	void Union(int regionIndex1, int regionIndex2, vector<REGIONDATASET *> *regions[], int index, int topIndex, int *regionMap, int maxPosition);
	void ExtractBlobs(unsigned short *image, vector<REGIONDATASET *> *regions[], int topIndex, int bottomIndex, vector<CBlobData *> *blobList);
	bool HasBeenVisisted(int reference, vector<int> *visited);
	void GetHighestIntensityPoint(unsigned short *image, INTENSITYBLOBPAIR *topBlob, int *intensity, int *intensityPosition);

public:
	CMSERDetector();
	virtual ~CMSERDetector();
	void GetBlobs(unsigned short *image, vector<CBlobData *> *blobList);
};
