#include "stdafx.h"
#include "MSERDetector.h"
#include "CellFinder.h"

#define BLOBCOUNT_THRESHOLD	  20
#define PIXELCOUNT_THRESHOLD  3000
#define CPI_MIN	200
#define CPI_MAX 400

CMSERDetector::CMSERDetector()
{
}

CMSERDetector::~CMSERDetector()
{
}

template<typename T>
void CMSERDetector::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CMSERDetector::Partition(vector<INTENSITY_PIXEL_SET *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	unsigned short intensity = (*list)[position]->intensity;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->intensity < intensity)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CMSERDetector::ChoosePivot(vector<INTENSITY_PIXEL_SET *> *list, int lo, int hi)
{
	int pivot = lo;
	unsigned short intensity0 = (*list)[lo]->intensity;
	unsigned short intensity1 = (*list)[(lo + hi) / 2]->intensity;
	unsigned short intensity2 = (*list)[hi]->intensity;
	if ((intensity0 <= intensity1) && (intensity1 <= intensity2))
		pivot = (lo + hi) / 2;
	else if ((intensity0 <= intensity2) && (intensity2 <= intensity1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
};

int CMSERDetector::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CMSERDetector::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
};

template<typename T>
void CMSERDetector::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CMSERDetector::GetBlobs(unsigned short *image, vector<CBlobData *> *blobList)
{
	vector<INTENSITY_PIXEL_SET *> intensityList;
	int width = SAMPLEFRAMEWIDTH;
	int height = SAMPLEFRAMEHEIGHT;
	vector<CBlobData *> artifactBlobs;
	CCellFinder::GetSortedPixelList(image, &artifactBlobs, &intensityList);
	int topIndex = (int)intensityList.size() - 1;
	int bottomIndex = 0;
	
	vector<REGIONDATASET *> **regions;
	regions = new vector<REGIONDATASET *> *[intensityList.size()];
	memset(regions, 0, sizeof(vector<REGIONDATASET *> *) * intensityList.size());
	unsigned char *placedMap = new unsigned char[width * height];
	memset(placedMap, 0, sizeof(unsigned char) * width * height);
	int *regionMap = new int[width * height];
	memset(regionMap, 0, sizeof(int) * width * height);
	unsigned short threshold = 0;
	int maxPosition = width * height;
	
	try
	{
		int regionReference = 0;
		for (int i = topIndex; i > bottomIndex; i--)
		{
			INTENSITY_PIXEL_SET *intenPtr = intensityList[i];
			threshold = intenPtr->intensity;
			regions[i] = new vector<REGIONDATASET *>();
			if (i < topIndex)
			{
				bool atBackground = false;
				if ((int)regions[i + 1]->size() >= BLOBCOUNT_THRESHOLD)
					atBackground = true;
				else
				{
					for (int j = 0; j < (int)regions[i + 1]->size(); j++)
					{
						if ((int)(*regions[i + 1])[j]->pixels->size() > PIXELCOUNT_THRESHOLD)
						{
							atBackground = true;
							break;
						}
						REGIONDATASET *region = new REGIONDATASET((*regions[i + 1])[j], threshold);
						(*regions[i + 1])[j]->child = region;
						regions[i]->push_back(region);
					}
				}
				if (atBackground)
				{
					bottomIndex = i;
					break;
				}
			}

			for (int j = 0; j < (int)intenPtr->pixelList->size(); j++)
			{
				int position = (int)(*intenPtr->pixelList)[j];
				placedMap[position] = (unsigned char)1;
				int regionIndex1 = -1;
				int regionIndexPosition = 0;
				if (((position - 1) >= 0) && (placedMap[position - 1] == (unsigned char)1))
				{
					regionIndex1 = Find((position - 1), regionMap, &regionIndexPosition, maxPosition);
					if (regionIndex1 > -1)
					{
						regionMap[position] = regionIndexPosition;
						(*regions[i])[regionIndex1]->pixels->push_back(position);
					}
				}
				if (((position + 1) < maxPosition) && (placedMap[position + 1] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position + 1), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position + 1, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position + 1), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (((position - width) >= 0) && (placedMap[position - width] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position - width), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position - width, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position - width), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (((position + width) < maxPosition) && (placedMap[position + width] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position + width), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position + width, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position + width), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (regionIndex1 == -1)
				{
					regionReference++;
					REGIONDATASET *region = new REGIONDATASET(regionReference, position, threshold);
					regions[i]->push_back(region);
					int regionIndex = (int) regions[i]->size();
					regionMap[position] = -regionIndex;
				}
			}
		}
	}
	catch (...)
	{
		TRACE(_T("Exception Throwed in Find()"));
	}

	for (int i = topIndex; i >= 0; i--)
		delete intensityList[i];
	intensityList.clear();

	delete[] placedMap;
	delete[] regionMap;

	ExtractBlobs(image, regions, topIndex, bottomIndex, blobList);

	for (int i = 0; i < (int)blobList->size(); i++)
	{
		CBlobData *blob = (*blobList)[i];
		for (int j = 0; j < (int)artifactBlobs.size(); j++)
		{
			CBlobData *blob1 = artifactBlobs[j];
			for (int k = 0; k < (int)blob1->m_Artifact->size(); k++)
				blob->m_Artifact->push_back((*blob1->m_Artifact)[k]);
		}
	}
	for (int i = 0; i < (int)artifactBlobs.size(); i++)
	{
		delete artifactBlobs[i];
		artifactBlobs[i] = NULL;
	}
	artifactBlobs.clear();
	for (int index = topIndex; index >= 0; index--)
	{
		if (regions[index] != NULL)
		{
			for (int regionIndex = 0; regionIndex < (int)regions[index]->size(); regionIndex++)
			{
				delete (*regions[index])[regionIndex];
				(*regions[index])[regionIndex] = NULL;
			}
			regions[index]->clear();
			delete regions[index];
			regions[index] = NULL;
		}
	}

	delete[] regions;
}

int CMSERDetector::Find(int position, int *regionMap, int *regionLocation, int maxPosition)
{
	int result = regionMap[position];
	if (result < 0)
	{
		*regionLocation = position + 1;
		result = -result;
		result--;
	}
	else if (result > 0)
	{
		*regionLocation = result;
		int regionIndexPosition = result - 1;
		if ((regionIndexPosition >= 0) && (regionIndexPosition < maxPosition))
		{
			result = regionMap[regionIndexPosition];
			if (result < 0)
			{
				result = -result;
				result--;
			}
			else
				throw 253;
		}
	}
	else
		throw 254;

	return result;
}

void CMSERDetector::Union(int regionIndex1, int regionIndex2, vector<REGIONDATASET *> *regions[], int index, int topIndex, int *regionMap, int maxPosition)
{
	int bigRegionIndex = regionIndex1;
	int smallRegionIndex = regionIndex2;
	vector<REGIONDATASET *> *rgnPtr = regions[index];
	if ((*regions[index])[regionIndex2]->pixels->size() > (*regions[index])[regionIndex1]->pixels->size())
	{
		bigRegionIndex = regionIndex2;
		smallRegionIndex = regionIndex1;
	}

	if ((*regions[index])[bigRegionIndex]->reference > (*regions[index])[smallRegionIndex]->reference)
		(*regions[index])[bigRegionIndex]->reference = (*regions[index])[smallRegionIndex]->reference;

	int regionReference = (*regions[index])[bigRegionIndex]->reference;

	for (int i = 0; i < (int)(*regions[index])[smallRegionIndex]->pixels->size(); i++)
		(*regions[index])[bigRegionIndex]->pixels->push_back((*(*regions[index])[smallRegionIndex]->pixels)[i]);
	if (index < topIndex)
	{
		for (int i = 0; i < (int)regions[index + 1]->size(); i++)
		{
			if ((*regions[index + 1])[i]->child == (*regions[index])[smallRegionIndex])
				(*regions[index + 1])[i]->child = (*regions[index])[bigRegionIndex];
		}
	}
	delete (*regions[index])[smallRegionIndex];
	(*regions[index])[smallRegionIndex] = NULL;
	regions[index]->erase(regions[index]->begin() + smallRegionIndex);
	for (int i = 0; i < (int)regions[index]->size(); i++)
	{
		int newPosition = (*regions[index])[i]->position;
		regionMap[newPosition] = -(i + 1);
		if (regionReference == (*regions[index])[i]->reference)
		{
			for (int j = 0; j < (int)(*regions[index])[i]->pixels->size(); j++)
			{
				int position = (*(*regions[index])[i]->pixels)[j];
				if (position != newPosition)
					regionMap[position] = newPosition + 1;
			}
		}
	}
}

void CMSERDetector::ExtractBlobs(unsigned short *image, vector<REGIONDATASET *> *regions[], int topIndex, int bottomIndex, vector<CBlobData *> *blobList)
{
	vector<int> visited;
	for (int index = topIndex; index > bottomIndex; index--)
	{
		for (int regionIndex = 0; regionIndex < (int)regions[index]->size(); regionIndex++)
		{
			if ((!HasBeenVisisted((*regions[index])[regionIndex]->reference, &visited)) && ((int)(*regions[index])[regionIndex]->pixels->size() > DEFAULT_MIN_BLOBPIXELS))
			{
				visited.push_back((*regions[index])[regionIndex]->reference);
				vector<INTENSITYBLOBPAIR *> tree;
				REGIONDATASET *ptr = (*regions[index])[regionIndex];
				INTENSITYBLOBPAIR *pair = new INTENSITYBLOBPAIR(ptr->intensity, ptr->pixels);
				int reference = ptr->reference;
				int index1 = index;
				if (ptr->child != NULL)
				{
					for (int i = 0; i < (int)regions[index1]->size(); i++)
					{
						if (((*regions[index1])[i]->child == ptr->child) && ((int)(*regions[index1])[i]->pixels->size() > DEFAULT_MIN_BLOBPIXELS))
						{
							if (((*regions[index1])[i]->reference != reference) && (!HasBeenVisisted((*regions[index1])[i]->reference, &visited)))
								visited.push_back((*regions[index1])[i]->reference);
							else
								continue;
							REGIONDATASET *ptr1 = (*regions[index1])[i];
							for (int j = 0; j < (int)ptr1->pixels->size(); j++)
								pair->pixels->push_back((*ptr1->pixels)[j]);
						}
					}	
				}
				tree.push_back(pair);

				while (ptr != NULL)
				{
					ptr = ptr->child;
					index1--;
					if (ptr != NULL)
					{
						bool found = false;
						for (int i = 0; i < (int)regions[index1]->size(); i++)
						{
							if (ptr == (*regions[index1])[i])
							{
								found = true;
								break;
							}
						}
						if (found)
						{
							if (ptr->child != NULL)
							{
								INTENSITYBLOBPAIR *pair = new INTENSITYBLOBPAIR(ptr->intensity, ptr->pixels);
								for (int i = 0; i < (int)regions[index1]->size(); i++)
								{
									if (((*regions[index1])[i]->child == ptr->child) && ((int)(*regions[index1])[i]->pixels->size() > DEFAULT_MIN_BLOBPIXELS))
									{
										if (((*regions[index1])[i]->reference != reference) && (!HasBeenVisisted((*regions[index1])[i]->reference, &visited)))
											visited.push_back((*regions[index1])[i]->reference);
										else
											continue;
										REGIONDATASET *ptr1 = (*regions[index1])[i];
										for (int j = 0; j < (int)ptr1->pixels->size(); j++)
											pair->pixels->push_back((*ptr1->pixels)[j]);
									}
								}
								tree.push_back(pair);
							}
						}
					}
				}

				if ((int)tree.size() > 0)
				{
					vector<INTENSITYBLOBPAIR *> tree1;
					int nextSize = 0;
					for (int j = 0; j<(int)tree.size(); j++)
					{
						INTENSITYBLOBPAIR *pair = tree[j];
						tree[j] = NULL;
						if (j == 0)
						{
							tree1.push_back(pair);
							nextSize = (int)pair->pixels->size() + BLOB_STACK_STEP;
						}
						else if ((int)pair->pixels->size() >= nextSize)
						{
							nextSize = (int)pair->pixels->size() + BLOB_STACK_STEP;
							tree1.push_back(pair);
						}
						else
							delete pair;
					}
					tree.clear();
					CBlobData *blob = new CBlobData(SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
					blobList->push_back(blob);
					GetHighestIntensityPoint(image, tree1[0], &blob->m_Intensity, &blob->m_IntensityPos);
					int index = (int)tree1.size() / 2;
					blob->m_Threshold = tree1[index]->intensity;
					for (int j = 0; j < (int)tree1[index]->pixels->size(); j++)
						blob->m_Pixels->push_back((*tree1[index]->pixels)[j]);
					QuickSort(blob->m_Pixels, 0, (int)blob->m_Pixels->size() - 1);
					for (int j = 0; j < (int)tree1.size(); j++)
					{
						delete tree1[j];
						tree1[j] = NULL;
					}
					tree1.clear();
				}
			}
		}
	}
	visited.clear();
}

bool CMSERDetector::HasBeenVisisted(int reference, vector<int> *visited)
{
	bool isVisited = false;
	for (int i = 0; i < (int)visited->size(); i++)
	{
		if (reference == (*visited)[i])
		{
			isVisited = true;
			break;
		}
	}
	return isVisited;
}

void CMSERDetector::GetHighestIntensityPoint(unsigned short *image, INTENSITYBLOBPAIR *topBlob, int *intensity, int *intensityPosition)
{
	int maxIntensity = 0;
	for (int i = 0; i < (int)topBlob->pixels->size(); i++)
	{
		int pos = (*topBlob->pixels)[i];
		if (image[pos] > maxIntensity)
		{
			maxIntensity = image[pos];
			*intensityPosition = pos;
		}
	}
	*intensity = maxIntensity;
}
