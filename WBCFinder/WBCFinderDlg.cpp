﻿
// WBCFinderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WBCFinder.h"
#include "WBCFinderDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"
#include "shlwapi.h"
#pragma comment(lib, "shlwapi.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CWBCFinderDlg dialog
#define IMAGE_WINDOW_X0 15
#define IMAGE_WINDOW_Y0 54
#define IMAGE_WINDOW_X1 1028
#define IMAGE_WINDOW_Y1 900

#define NUM_IMAGE_COLUMNS	4
#define NUM_IMAGE_ROWS		4
#define NUM_IMAGES			16
#define WM_MY_MESSAGE (WM_USER+1001)
#define WM_ENABLE_BUTTONS (WM_USER+1002)
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

const CString SAVE_REGION = _T("Save Region");
const CString BATCH_RUN = _T("Batch Run");

static DWORD WINAPI WBCScreenOperation(LPVOID param);
static DWORD WINAPI BatchRunOperation(LPVOID param);

CWBCFinderDlg::CWBCFinderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWBCFinderDlg::IDD, pParent)
	, m_Status(_T(""))
	, m_ImageFilename(_T(""))
	, m_HitIndex(0)
	, m_TotalHits(0)
	, m_FullImagePathname(_T(""))
	, m_StartingIndices(_T(""))
	, m_WBCGreenRelative(0)
	, m_WBCGreenCount(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWBCFinderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_Display);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_IMAGEFILE, m_ImageFilename);
	DDX_Text(pDX, IDC_HITIDX, m_HitIndex);
	DDX_Text(pDX, IDC_TOTALHITS, m_TotalHits);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_REDCOLOR, m_Red);
	DDX_Text(pDX, IDC_GCUTOFF, m_TwoThresholds);
	DDX_Control(pDX, IDC_FRAMESONLY, m_ScanFramesOnly);
	DDX_Text(pDX, IDC_INDICES, m_StartingIndices);
	DDX_Text(pDX, IDC_WBCGREEN, m_WBCGreenRelative);
	DDX_Control(pDX, IDC_DISPLAYHITS, m_DisplayHit);
}

BEGIN_MESSAGE_MAP(CWBCFinderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CWBCFinderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CWBCFinderDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_NEXTHIT, &CWBCFinderDlg::OnBnClickedNexthit)
	ON_BN_CLICKED(IDC_PREVHIT, &CWBCFinderDlg::OnBnClickedPrevhit)
	ON_BN_CLICKED(IDC_GOTOHIT, &CWBCFinderDlg::OnBnClickedGotohit)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(WM_ENABLE_BUTTONS, OnEnableButtons)
	ON_BN_CLICKED(IDC_REDCOLOR, &CWBCFinderDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREEN, &CWBCFinderDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_BLUE, &CWBCFinderDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_SHOW, &CWBCFinderDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_SAVERGN, &CWBCFinderDlg::OnBnClickedSavergn)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CWBCFinderDlg message handlers

BOOL CWBCFinderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CellRegionFinder(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	m_HitFinder.m_Log = &m_Log;
	SetWindowText(version);
	m_ZeissData = false;
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version, &m_TwoThresholds);
	LoadDefaultSettings();
	m_ScanFramesOnly.SetCheck(BST_UNCHECKED);
	m_RedCPI = 250;
	m_GreenCPI = 250;
	m_BlueCPI = 250;
	m_NumCTC1 = 0;
	m_NumCTC2 = 0;
	m_NumNONCTC = 0;
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Status = _T("Please Load Image");
	UpdateData(FALSE);
	m_Red.SetCheck(BST_CHECKED);
	m_Green.SetCheck(BST_CHECKED);
	m_Blue.SetCheck(BST_CHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	CButton *btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->SetWindowTextW(BATCH_RUN);
	m_RedPatchImage = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
	m_GreenPatchImage = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
	m_BluePatchImage = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
	m_DisplayHit.SetCheck(BST_UNCHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWBCFinderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWBCFinderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWBCFinderDlg::PaintImages()
{
	if (m_Image == NULL)
		return;
	else if ((m_Image.GetWidth() == m_RedTIFFData->GetSubsampledWidth()) && (m_Image.GetHeight() == m_RedTIFFData->GetSubsampledHeight()))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
	}
	else if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
		CDC* pDC = m_Display.GetDC();
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = pDC->SelectObject(&CursorPen);
		int blockWidth = (rect.right - rect.left + 1) / NUM_IMAGE_COLUMNS;
		for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
		{
			pDC->MoveTo(rect.left + i * blockWidth, rect.top);
			pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
		}
		int blockHeight = (rect.bottom - rect.top + 1) / NUM_IMAGE_ROWS;
		for (int i = 1; i < NUM_IMAGE_ROWS; i++)
		{
			pDC->MoveTo(rect.left, rect.top + i * blockHeight);
			pDC->LineTo(rect.right, rect.top + i * blockHeight);
		}
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextAlign(TA_LEFT);
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int length10um = 15 * blockWidth / 100;
		for (int i = 0; i < NUM_IMAGES; i++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				int ii = i / NUM_IMAGE_COLUMNS;
				int jj = i % NUM_IMAGE_COLUMNS;
				CRect rect1;
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (startIndex + 1));
				if ((m_RGNDataList[startIndex]->GetColorCode() == CTC) ||
					(m_RGNDataList[startIndex]->GetColorCode() == CTC2))
					pDC->SetTextColor(RGB(255, 0, 0));
				else
					pDC->SetTextColor(RGB(0, 255, 0));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 190;
				pDC->MoveTo(rect1.left, rect1.top);
				pDC->LineTo(rect1.left + length10um, rect1.top);
				pDC->SetTextColor(RGB(255, 255, 255));
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 170;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("10um"));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(255, 0, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("R:%d"), m_RGNDataList[startIndex]->m_RedValue);
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(0, 255, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 30;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("G:%d"), m_RGNDataList[startIndex]->m_GreenValue);
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(255, 255, 255));
				rect1.left = rect.left + jj * blockWidth + 150;
				rect1.top = rect.top + ii * blockHeight + 50;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("ratio1:%.2lf"), m_RGNDataList[startIndex]->m_Ratio1);
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				if (startIndex < m_NumCTC2)
				{
					rect1.left = rect.left + jj * blockWidth + 150;
					rect1.top = rect.top + ii * blockHeight + 70;
					rect1.right = rect1.left + 200;
					rect1.bottom = rect1.top + 20;
					label.Format(_T("ratio2:%.2lf"), m_RGNDataList[startIndex]->m_Ratio2);
					pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				}
			}
		}
		pDC->SelectObject(pOldPen);
	}
}

void CWBCFinderDlg::UpdateImageDisplay()
{
	RECT rect;
	m_Display.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CWBCFinderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	delete m_RedPatchImage;
	delete m_GreenPatchImage;
	delete m_BluePatchImage;
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	CDialogEx::OnCancel();
}


void CWBCFinderDlg::OnBnClickedLoad()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	CButton *btn = NULL;
	EnableButtons(FALSE);
	m_ImageFilename = _T("");
	m_FullImagePathname = _T("");
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	m_HitIndex = 0;
	m_NumCTC1 = 0;
	m_NumCTC2 = 0;
	m_NumNONCTC = 0;
	m_WBCGreenRelative = 0;
	m_StartingIndices = _T("");
	if (m_Image != NULL)
		m_Image.Destroy();
	m_Status = _T("Loading and Processing Image, Please wait...");
	UpdateData(FALSE);

	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Red Channel TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		btn = (CButton *)GetDlgItem(IDC_SAVERGN);
		btn->SetWindowTextW(SAVE_REGION);
		CString filename = dlg.GetPathName();
		m_FullImagePathname = filename;
		m_ImageFilename = dlg.GetFileName();
		UpdateData(FALSE);
		if (m_ImageFilename.Find(_T(".tif")) > 0)
		{
			HANDLE thread = ::CreateThread(NULL, 0, WBCScreenOperation, this, CREATE_SUSPENDED, NULL);

			if (!thread)
			{
				AfxMessageBox(_T("Fail to create thread for screening"));
				message.Format(_T("Failed to creat a thread for screening"));
				m_Log.Message(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				return;
			}

			::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
			::ResumeThread(thread);
			message.Format(_T("Launched a thread for screening"));
			m_Log.Message(message);
			m_Status.Format(_T("Started Image Screening for finding Cell Regions"));
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	else
	{
		btn = (CButton *)GetDlgItem(IDC_LOAD);
		btn->EnableWindow(TRUE);
	}
}

void CWBCFinderDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CWBCFinderDlg::CopyToRGBImage()
{
	if (m_Image == NULL)
		return;
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		if ((m_HitIndex < 1) && (m_HitIndex > m_TotalHits))
			return;
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nPitch = m_Image.GetPitch();
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		for (int ImageLocationIndex = 0; ImageLocationIndex < NUM_IMAGES; ImageLocationIndex++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				CRGNData *rgnPtr = m_RGNDataList[startIndex];
				int regionWidth = rgnPtr->GetWidth();
				int regionHeight = rgnPtr->GetHeight();
				
				unsigned short *pBlueBuffer = m_BluePatchImage;
				unsigned short *pGreenBuffer = m_GreenPatchImage;
				unsigned short *pRedBuffer = m_RedPatchImage;
				
				int x0, y0;
				rgnPtr->GetPosition(&x0, &y0);
				m_RedTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, m_RedPatchImage);
				m_GreenTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, m_GreenPatchImage);
				m_BlueTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, m_BluePatchImage);

				int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
				if (rgnPtr->m_BlueFrameMax > 0)
					blueMax = rgnPtr->m_BlueFrameMax;
				int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
				if (rgnPtr->m_RedFrameMax > 0)
					redMax = rgnPtr->m_RedFrameMax;
				int greenMax = rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR);
				if (rgnPtr->m_GreenFrameMax > 0)
					greenMax = rgnPtr->m_GreenFrameMax;

				BYTE bluePixelValue = 0;
				BYTE greenPixelValue = 0;
				BYTE redPixelValue = 0;

				for (int y = 0; y < regionHeight; y++)
				{
					for (int x = 0; x < regionWidth; x++)
					{
						if (m_Blue.GetCheck() == BST_CHECKED)
							bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
						else
							bluePixelValue = (BYTE)0;
						if (m_Green.GetCheck() == BST_CHECKED)
							greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
						else
							greenPixelValue = (BYTE)0;
						if (m_Red.GetCheck() == BST_CHECKED)
							redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
						else
							redPixelValue = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
					}
				}

				if ((m_ShowBoundary.GetCheck() == BST_CHECKED) && (rgnPtr->GetBlobData(RB_COLORS)->size() > 0))
				{
					CBlobData *blob = NULL;
					blob = (*rgnPtr->GetBlobData(RB_COLORS))[0];
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
						int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
					}
				}
			}
			else
			{
				for (int y = 0; y < SAMPLEFRAMEHEIGHT; y++)
				{
					for (int x = 0; x < SAMPLEFRAMEWIDTH; x++)
					{
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = (BYTE)0;
					}
				}
			}
		}
	}
	else if ((m_Image.GetWidth() == m_RedTIFFData->GetSubsampledWidth()) && (m_Image.GetHeight() == m_RedTIFFData->GetSubsampledHeight()))
	{
		int width = m_RedTIFFData->GetSubsampledWidth();
		int height = m_RedTIFFData->GetSubsampledHeight();
		unsigned short *redImg = new unsigned short[width * height];
		unsigned short *greenImg = new unsigned short[width * height];
		unsigned short *blueImg = new unsigned short[width * height];
		bool retRed = m_RedTIFFData->GetSubsampleImage(redImg);
		bool retGreen = m_GreenTIFFData->GetSubsampleImage(greenImg);
		bool retBlue = m_BlueTIFFData->GetSubsampleImage(blueImg);
		int redCutoff = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF];
		int redContrast = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST];
		int greenCutoff = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF];
		int greenContrast = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST];
		int blueCutoff = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF];
		int blueContrast = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST];
		if (retRed && retGreen && retBlue)
		{
			BYTE *ptr = (BYTE *)m_Image.GetBits();
			int stride = m_Image.GetPitch() - 3 * width;
			unsigned short *ptrRed = redImg;
			unsigned short *ptrGreen = greenImg;
			unsigned short *ptrBlue = blueImg;
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++, ptrRed++, ptrGreen++, ptrBlue++)
				{
					BYTE redByte = GetContrastEnhancedByte(*ptrRed, redContrast, redCutoff);
					BYTE greenByte = GetContrastEnhancedByte(*ptrGreen, greenContrast, greenCutoff);
					BYTE blueByte = GetContrastEnhancedByte(*ptrBlue, blueContrast, blueCutoff);
					*ptr++ = blueByte;
					*ptr++ = greenByte;
					*ptr++ = redByte;
				}
				if (stride > 0)
					ptr += stride;
			}
		}
		delete[] redImg;
		delete[] greenImg;
		delete[] blueImg;
	}
}

BYTE CWBCFinderDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CWBCFinderDlg::OnBnClickedNexthit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(FALSE);
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int totalPages = (m_TotalHits / NUM_IMAGES) + 1;
		if ((startIndex + NUM_IMAGES) <= (totalPages * NUM_IMAGES))
		{
			m_HitIndex += NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), startIndex + NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
	if (m_HitIndex > NUM_IMAGES)
	{
		btn = (CButton *)GetDlgItem(IDC_PREVHIT);
		btn->EnableWindow(TRUE);
	}
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(TRUE);
}


void CWBCFinderDlg::OnBnClickedPrevhit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		if (startIndex >= NUM_IMAGES)
		{
			m_HitIndex -= NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			CButton *btn = (CButton *)GetDlgItem(IDC_PREVHIT);
			btn->EnableWindow(FALSE);
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex - NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}


void CWBCFinderDlg::OnBnClickedGotohit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_HitIndex > 0) && (m_HitIndex <= m_TotalHits))
	{
		CopyToRGBImage();
		UpdateImageDisplay();
		m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
		UpdateData(FALSE);
	}
	else
	{
		if ((m_Image != NULL) &&
			(m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}

BOOL CWBCFinderDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		UpdateData(TRUE);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CWBCFinderDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

LRESULT CWBCFinderDlg::OnEnableButtons(WPARAM wparam, LPARAM lparam)
{
	EnableButtons(TRUE);
	return 0;
}

void CWBCFinderDlg::SaveRegionData(CString pathname, CString filename)
{
	if ((m_RGNDataList.size() > 0) && (m_DisplayHit.GetCheck() == BST_UNCHECKED))
	{
		SaveRegionFile(pathname, filename);
		int index = filename.Find(_T(".rgn"));
		filename = filename.Mid(0, index);
		CString regionFileName;
		regionFileName.Format(_T("%s\\%s.csv"), pathname, filename);
		CStdioFile theFile;
		CString textline;
		if (theFile.Open(regionFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("SampleName=%s RedCPI=%d GreenCPI=%d BlueCPI=%d Scan9FramesOnly=%s WBCGreenAvg=%d WBCGreenRelative=%d WBCGreenCount=%d WBCGreenMarkup=%d TwoThresholds=%s Count1=%d Count2=%d Count3=%d\n"),
				m_ImageFilename, m_RedCPI, m_GreenCPI, m_BlueCPI, ((m_ScanFramesOnly.GetCheck() == BST_CHECKED) ? _T("YES") : _T("NO")),
				m_CTCParams.m_WBCGreenAvg, m_WBCGreenRelative, m_WBCGreenCount, m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP], m_TwoThresholds, m_NumCTC1, m_NumCTC2 - m_NumCTC1, (int)m_RGNDataList.size() - m_NumCTC2);
			theFile.WriteString(textline);
			textline.Format(_T("RgnIdx,X0,Y0,ColorCode,CellPixels,RedRelative,RedBackground,GreenRelative,GreenBackground,GreenPixelCount,Ratio1,Ratio2\n"));
			theFile.WriteString(textline);
			int greenCutoff = m_GreenCPI + m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP];
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				CRGNData *rgnPtr = m_RGNDataList[i];
				int x0, y0;
				rgnPtr->GetPosition(&x0, &y0);
				int cellPixels = rgnPtr->GetPixels(RB_COLORS);
				vector<int> *positions = NULL;
				if ((rgnPtr->GetBlobData(RB_COLORS)->size() > 0) && ((*rgnPtr->GetBlobData(RB_COLORS))[0] != NULL))
				{
					positions = (*rgnPtr->GetBlobData(RB_COLORS))[0]->m_Pixels;
				}
				if (rgnPtr->GetPixels(GB_COLORS) > cellPixels)
				{
					cellPixels = rgnPtr->GetPixels(GB_COLORS);
					if ((rgnPtr->GetBlobData(GB_COLORS)->size() > 0) && ((*rgnPtr->GetBlobData(GB_COLORS))[0] != NULL))
					{
						positions = (*rgnPtr->GetBlobData(GB_COLORS))[0]->m_Pixels;
					}
				}
				int greenPixelCount = 0;
				if (m_GreenTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, m_GreenPatchImage))
				{
					for (int j = 0; j < (int)positions->size(); j++)
					{
						int position = (*positions)[j];
						int intensity = m_GreenPatchImage[position];
						if (intensity > greenCutoff)
							greenPixelCount++;
					}
				}
				rgnPtr->SetPixels(GREEN_COLOR, greenPixelCount);
				if (i < m_NumCTC2)
				{
					textline.Format(_T("%d,%d,%d,%s,%d,%d,%d,%d,%d,%d,%.2lf,%.2lf\n"), i + 1, x0, y0, ((rgnPtr->GetColorCode() == CTC) ? _T("CTC") : _T("NONCTC")),
						cellPixels, rgnPtr->m_RedValue, rgnPtr->GetCPI(RED_COLOR), rgnPtr->m_GreenValue, rgnPtr->GetCPI(GREEN_COLOR), rgnPtr->GetPixels(GREEN_COLOR), 
						rgnPtr->m_Ratio1, rgnPtr->m_Ratio2);
				}
				else
				{
					textline.Format(_T("%d,%d,%d,%s,%d,%d,%d,%d,%d,%d,%.2lf\n"), i + 1, x0, y0, ((rgnPtr->GetColorCode() == CTC) ? _T("CTC") : _T("NONCTC")),
						cellPixels,rgnPtr->m_RedValue, rgnPtr->GetCPI(RED_COLOR), rgnPtr->m_GreenValue, rgnPtr->GetCPI(GREEN_COLOR), rgnPtr->GetPixels(GREEN_COLOR), 
						rgnPtr->m_Ratio1);
				}
				theFile.WriteString(textline);
			}
			theFile.Close();
		}
		regionFileName.Format(_T("%s\\%s.wb3"), pathname, filename);
		if (theFile.Open(regionFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("WBCGreenAverage=%d\n"), m_CTCParams.m_WBCGreenAvg);
			theFile.WriteString(textline);
			theFile.Close();
		}
		regionFileName.Format(_T("%s\\%s.wb4"), pathname, filename);
		if (theFile.Open(regionFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("WBCGreenRelative=%d\n"), m_WBCGreenRelative);
			theFile.WriteString(textline);
			theFile.Close();
		}
	}
	else if ((m_RGNDataList.size() > 0) && (m_DisplayHit.GetCheck() == BST_CHECKED))
	{
		int index = filename.Find(_T(".rgn"));
		filename = filename.Mid(0, index);
		CString filename1;
		CStdioFile theFile;

		if (m_NumCTC1 > 0)
		{
			filename1.Format(_T("%s\\%s_1.rgn"), pathname, filename);
			if (theFile.Open(filename1, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
			{
				for (int i = 0; i < m_NumCTC1; i++)
				{
					CString singleLine;
					int x0, y0;
					m_RGNDataList[i]->GetPosition(&x0, &y0);
					singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
						m_RGNDataList[i]->GetColorCode(), x0, y0, i + 1);
					theFile.WriteString(singleLine);
				}
				theFile.Close();
			}
		}
	}
}

void CWBCFinderDlg::GetWBCGreenValues()
{
	m_CTCParams.m_WBCGreenAvg = 0;
	m_WBCGreenRelative = 0;
	m_WBCGreenCount = 0;
	int wbcCount = 0;
	int wbcGreenSum = 0;
	int wbcGreenSum1 = 0;
	int wbcCountPercentage = m_CTCParams.m_CTCParams[(int)PARAM_REDTHRESHOLD_DROP];
	int ratio1ThresholdIndex = (int) ((wbcCountPercentage * (double) m_RGNDataList.size()) / 100.0);
	double ratio1Threshold = (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST];
	if ((ratio1ThresholdIndex > 0) && (ratio1ThresholdIndex < (int)m_RGNDataList.size()))
	{
		ratio1Threshold = m_RGNDataList[ratio1ThresholdIndex]->m_Ratio1 + 0.2;
	}
	for (int i = 0; i < (int)m_RGNDataList.size(); i++)
	{
		if (m_RGNDataList[i]->m_Ratio1 <= ratio1Threshold)
		{
			wbcGreenSum1 += m_RGNDataList[i]->m_GreenAverage;
			wbcGreenSum += m_RGNDataList[i]->m_GreenValue;
			wbcCount++;
		}
	}
	if (wbcCount > 0)
	{
		m_CTCParams.m_WBCGreenAvg = wbcGreenSum1 / wbcCount;
		m_WBCGreenRelative = wbcGreenSum / wbcCount;
		m_WBCGreenCount = wbcCount;
	}
}

DWORD WINAPI WBCScreenOperation(LPVOID param)
{
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	CString message;
	bool loaded = false;
	while (true)
	{
		int index = ptr->m_ImageFilename.Find(ptr->Leica_Red_Prefix, 0);
		if (index == -1)
		{
			index = ptr->m_ImageFilename.Find(ptr->Zeiss_Red_Postfix, 0);
			if (index == -1)
			{
				if (ptr->Zeiss_Red_Postfix.Find(_T("_c3_ORG.tif")) > -1)
				{
					index = ptr->m_ImageFilename.Find(_T("_c2_ORG.tif"));
				}
				else if (ptr->Zeiss_Red_Postfix.Find(_T("_c2_ORG.tif")) > -1)
				{
					index = ptr->m_ImageFilename.Find(_T("_c3_ORG.tif"));
				}
			}
		}
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), ptr->m_ImageFilename, ptr->Leica_Red_Prefix, ptr->Zeiss_Red_Postfix);
			AfxMessageBox(message);
			break;
		}
		bool ret = ptr->m_RedTIFFData->LoadRawTIFFFile(ptr->m_FullImagePathname);
		if (!ret)
		{
			message.Format(_T("Failed to load %s"), ptr->m_FullImagePathname);
			AfxMessageBox(message);
			break;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), ptr->m_FullImagePathname, ptr->m_RedTIFFData->GetCPI());
			ptr->m_Log.Message(message);
			index = ptr->m_ImageFilename.Find(ptr->Leica_Red_Prefix, 0);
			if (index == -1)
			{
				int postfixIndex = ptr->m_FullImagePathname.Find(ptr->Zeiss_Red_Postfix);
				if (postfixIndex == -1)
				{
					if (ptr->Zeiss_Red_Postfix.Find(_T("_c3_ORG.tif")) > -1)
					{
						postfixIndex = ptr->m_FullImagePathname.Find(_T("_c2_ORG.tif"));
					}
					else if (ptr->Zeiss_Red_Postfix.Find(_T("_c2_ORG.tif")) > -1)
					{
						postfixIndex = ptr->m_FullImagePathname.Find(_T("_c3_ORG.tif"));
					}
				}
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), ptr->m_FullImagePathname, ptr->Leica_Red_Prefix, ptr->Zeiss_Red_Postfix);
					AfxMessageBox(message);
					break;
				}
				else
				{
					CString samplePathName = ptr->m_FullImagePathname.Mid(0, postfixIndex);
					postfixIndex = ptr->m_ImageFilename.Find(_T(".tif"));
					CString sampleName = ptr->m_ImageFilename.Mid(0, postfixIndex);
					ptr->m_ImageFilename = sampleName;
					CString filename2 = samplePathName + ptr->Zeiss_Green_Postfix;
					ret = ptr->m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, ptr->m_GreenTIFFData->GetCPI());
						ptr->m_Log.Message(message);
						filename2 = samplePathName + ptr->Zeiss_Blue_Postfix;
						ret = ptr->m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							ptr->m_ZeissData = true;
							message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, ptr->m_BlueTIFFData->GetCPI());
							ptr->m_Log.Message(message);
							message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
							ptr->m_CTCParams.LoadCTCParams(message, &ptr->m_TwoThresholds);
							loaded = true;
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							AfxMessageBox(message);
							break;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						AfxMessageBox(message);
						break;
					}
				}
			}
			else
			{
				CString postfix = ptr->m_ImageFilename.Mid(ptr->Leica_Red_Prefix.GetLength());
				index = ptr->m_FullImagePathname.Find(ptr->Leica_Red_Prefix, 0);
				CString pathname = ptr->m_FullImagePathname.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, ptr->Leica_Green_Prefix, postfix);
				ret = ptr->m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					AfxMessageBox(message);
					break;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, ptr->m_GreenTIFFData->GetCPI());
					ptr->m_Log.Message(message);
					filename2.Format(_T("%s%s%s"), pathname, ptr->Leica_Blue_Prefix, postfix);
					ret = ptr->m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						AfxMessageBox(message);
						break;
					}
					else
					{
						ptr->m_ZeissData = false;
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, ptr->m_BlueTIFFData->GetCPI());
						ptr->m_Log.Message(message);
						message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
						ptr->m_CTCParams.LoadCTCParams(message, &ptr->m_TwoThresholds);
						loaded = true;
						index = postfix.Find(_T(".tif"));
						ptr->m_ImageFilename = postfix.Mid(0, index);
					}
				}
			}
		}

		if (loaded)
		{
			ptr->m_RedCPI = ptr->m_RedTIFFData->GetCPI();
			ptr->m_GreenCPI = ptr->m_GreenTIFFData->GetCPI();
			ptr->m_BlueCPI = ptr->m_BlueTIFFData->GetCPI();
			if (ptr->m_DisplayHit.GetCheck() == BST_UNCHECKED)
			{
				int width = ptr->m_RedTIFFData->GetSubsampledWidth();
				int height = ptr->m_RedTIFFData->GetSubsampledHeight();
				ptr->m_Image.Create(width, -height, 24);
				ptr->CopyToRGBImage();
				if (ptr->m_ScanFramesOnly.GetCheck() == BST_CHECKED)
				{
					BYTE *pCursor = (BYTE *)ptr->m_Image.GetBits();
					int pitch = ptr->m_Image.GetPitch();
					int centerX = ptr->m_RedTIFFData->GetImageWidth() / 2 - CAMERA_FRAME_WIDTH / 2;
					int centerY = ptr->m_RedTIFFData->GetImageHeight() / 2 - CAMERA_FRAME_HEIGHT / 2;
					for (int i = -1; i < 2; i++)
					{
						for (int j = -1; j < 2; j++)
						{
							int FrameX0 = (centerX + CAMERA_FRAME_WIDTH * 3 * j) / 32;
							int FrameY0 = (centerY + CAMERA_FRAME_HEIGHT * 3 * i) / 32;
							int FrameWidth = CAMERA_FRAME_WIDTH / 32;
							int FrameHeight = CAMERA_FRAME_HEIGHT / 32;
							for (int k = 0; k < FrameWidth; k++)
							{
								pCursor[pitch * FrameY0 + 3 * (FrameX0 + k)] = (BYTE)255;
								pCursor[pitch * FrameY0 + 3 * (FrameX0 + k) + 1] = (BYTE)255;
								pCursor[pitch * FrameY0 + 3 * (FrameX0 + k) + 2] = (BYTE)255;
								pCursor[pitch * (FrameY0 + FrameHeight) + 3 * (FrameX0 + k)] = (BYTE)255;
								pCursor[pitch * (FrameY0 + FrameHeight) + 3 * (FrameX0 + k) + 1] = (BYTE)255;
								pCursor[pitch * (FrameY0 + FrameHeight) + 3 * (FrameX0 + k) + 2] = (BYTE)255;
							}
							for (int k = 0; k < FrameHeight; k++)
							{
								pCursor[pitch * (FrameY0 + k) + 3 * FrameX0] = (BYTE)255;
								pCursor[pitch * (FrameY0 + k) + 3 * FrameX0 + 1] = (BYTE)255;
								pCursor[pitch * (FrameY0 + k) + 3 * FrameX0 + 2] = (BYTE)255;
								pCursor[pitch * (FrameY0 + k) + 3 * (FrameX0 + FrameWidth)] = (BYTE)255;
								pCursor[pitch * (FrameY0 + k) + 3 * (FrameX0 + FrameWidth) + 1] = (BYTE)255;
								pCursor[pitch * (FrameY0 + k) + 3 * (FrameX0 + FrameWidth) + 2] = (BYTE)255;
							}
						}
					}
					ptr->UpdateImageDisplay();
					for (int i = -1; i < 2; i++)
					{
						for (int j = -1; j < 2; j++)
						{
							int FrameX0 = centerX + CAMERA_FRAME_WIDTH * 3 * j;
							int FrameY0 = centerY + CAMERA_FRAME_HEIGHT * 3 * i;
							ptr->m_HitFinder.ScanSingleFrame(&ptr->m_CTCParams, ptr->m_RedTIFFData, ptr->m_GreenTIFFData, ptr->m_BlueTIFFData,
								&ptr->m_RGNDataList, FrameX0, FrameY0);
						}
					}
					ptr->m_HitFinder.SortRegionList(&ptr->m_RGNDataList);
				}
				else
				{
					ptr->UpdateImageDisplay();
					ptr->m_HitFinder.ScanImage(&ptr->m_CTCParams, ptr->m_RedTIFFData, ptr->m_GreenTIFFData, ptr->m_BlueTIFFData, &ptr->m_RGNDataList, false);
				}
			}
			else
			{
				if (ptr->ReadRgnfile())
				{
					for (int i = 0; i < (int)ptr->m_RGNDataList.size(); i++)
					{
						CRGNData *region = ptr->m_RGNDataList[i];
						region->SetImages(ptr->m_RedPatchImage, ptr->m_GreenPatchImage, ptr->m_BluePatchImage);
						int x0, y0;
						region->GetPosition(&x0, &y0);
						bool redRet = ptr->m_RedTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, ptr->m_RedPatchImage);
						bool greenRet = ptr->m_GreenTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, ptr->m_GreenPatchImage);
						bool blueRet = ptr->m_BlueTIFFData->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, ptr->m_BluePatchImage);
						if (redRet && greenRet && blueRet)
						{
							ptr->m_HitFinder.ProcessOneRegion(&ptr->m_CTCParams, region, ptr->m_ZeissData);
						}
						else
						{
							message.Format(_T("Failed to get patch images for Region %d"), i);
							ptr->m_Log.Message(message);
						}
						region->NullImages();
					}
				}
			}
			if (ptr->m_Image != NULL)
				ptr->m_Image.Destroy();
			ptr->m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
			ptr->m_TotalHits = (int)ptr->m_RGNDataList.size();
			ptr->m_HitIndex = 1;
			int start1, start2, start3;
			for (int i = 0; i < ptr->m_TotalHits; i++)
			{
				if (ptr->m_RGNDataList[i]->m_Ratio1 > (double)ptr->m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
				{
					double nominator = ptr->m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RGNDataList[i]->m_RedValue
						+ ptr->m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RGNDataList[i]->m_GreenValue;
					double denominator = ptr->m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RGNDataList[i]->m_RedValue
						+ ptr->m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RGNDataList[i]->m_GreenValue + 1.0;
					if ((nominator > 0) && (denominator > 0))
						ptr->m_RGNDataList[i]->m_Ratio2 = nominator / denominator;
					else if (nominator <= 0)
						ptr->m_RGNDataList[i]->m_Ratio2 = 0;
					else
					{
						ptr->m_RGNDataList[i]->m_Ratio2 = nominator;
					}
					ptr->m_NumCTC2 = i + 1;
				}
				else
					break;
			}
			if (ptr->m_DisplayHit.GetCheck() == BST_UNCHECKED)
			{
				if (ptr->m_NumCTC2 > 0)
				{
					ptr->QuickSort(&ptr->m_RGNDataList, 0, ptr->m_NumCTC2 - 1);
					for (int i = 0; i < ptr->m_NumCTC2; i++)
					{
						if (ptr->m_RGNDataList[i]->m_Ratio2 < (double)ptr->m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
							ptr->m_NumCTC1 = i + 1;
						else
							break;
					}
				}
				start1 = 0;
				if (ptr->m_NumCTC1 > 0)
					start1 = 1;
				start2 = 0;
				if (ptr->m_NumCTC2 > 0)
				{
					if (start1 == 0)
						start2 = 1;
					else
						start2 = ptr->m_NumCTC1 + 1;
				}
				start3 = 0;
				if (((int)ptr->m_RGNDataList.size() - ptr->m_NumCTC2) > 0)
				{
					if ((start1 == 0) && (start2 == 0))
						start3 = 1;
					else
						start3 = ptr->m_NumCTC2 + 1;
				}
			}
			else
			{
				ptr->m_NumCTC1 = (int)ptr->m_RGNDataList.size();
				ptr->m_NumCTC2 = (int)ptr->m_RGNDataList.size();
				start1 = 1;
				start2 = 0;
				start3 = 0;
			}
			ptr->m_StartingIndices.Format(_T("%d,%d,%d"), start1, start2, start3);
			ptr->GetWBCGreenValues();		
			::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ptr->CopyToRGBImage();
			ptr->UpdateImageDisplay();
			ptr->m_Status.Format(_T("Completed Image Screen and found %d cell regions"), ptr->m_TotalHits);
			::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			WCHAR *char1 = _T("\\");
			int fileStringIndex = ptr->m_FullImagePathname.ReverseFind(*char1);
			CString pathname = ptr->m_FullImagePathname.Mid(0, fileStringIndex);
			CString regionFileName;
			regionFileName.Format(_T("%s.rgn"), ptr->m_ImageFilename);
			ptr->SaveRegionData(pathname, regionFileName);
		}

		break;
	}
	::PostMessage(ptr->GetSafeHwnd(), WM_ENABLE_BUTTONS, NULL, NULL);
	return 0;
}

void  CWBCFinderDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREVHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GOTOHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SHOW);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_BLUE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREEN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDCOLOR);
	btn->EnableWindow(enable);
}


void CWBCFinderDlg::SaveRegionFile(CString pathname, CString filenameForSave)
{
	int index = filenameForSave.Find(_T(".rgn"));
	CString filename = filenameForSave.Mid(0, index);
	CString filename1;
	CStdioFile theFile;

	if (m_NumCTC1 > 0)
	{
		filename1.Format(_T("%s\\%s_1.rgn"), pathname, filename);
		if (theFile.Open(filename1, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = 0; i < m_NumCTC1; i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataList[i]->GetColorCode(), x0, y0, i + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
		}
	}

	if ((m_NumCTC2 - m_NumCTC1) > 0)
	{
		filename1.Format(_T("%s\\%s_2.rgn"), pathname, filename);
		if (theFile.Open(filename1, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = m_NumCTC1; i < m_NumCTC2; i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataList[i]->GetColorCode(), x0, y0, i - m_NumCTC1 + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
		}
	}

	if (((int)m_RGNDataList.size() - m_NumCTC2) > 0)
	{
		filename1.Format(_T("%s\\%s_3.rgn"), pathname, filename);
		if (theFile.Open(filename1, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = m_NumCTC2; i < (int)m_RGNDataList.size(); i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataList[i]->GetColorCode(), x0, y0, i - m_NumCTC2 + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
		}
	}

	if ((int)m_RGNDataList.size() > 0)
	{
		filename1.Format(_T("%s\\%s_all.rgn"), pathname, filename);
		if (theFile.Open(filename1, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataList[i]->GetColorCode(), x0, y0, i+1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
		}
	}
}

void CWBCFinderDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CWBCFinderDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	EnableButtons(FALSE);
	CButton *btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	CString buttonName;
	btn->GetWindowTextW(buttonName);
	if (buttonName.Find(SAVE_REGION) > -1)
	{
		int start1, start2, start3;
		if (m_DisplayHit.GetCheck() == BST_UNCHECKED)
		{
			m_TotalHits = (int)m_RGNDataList.size();
			m_HitIndex = 1;
			m_NumCTC1 = 0;
			m_NumCTC2 = 0;
			m_NumNONCTC = 0;
			GetWBCGreenValues();
			for (int i = 0; i < m_TotalHits; i++)
			{
				if (m_RGNDataList[i]->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
					m_NumCTC2 = i + 1;
				else
					break;
			}
			if (m_NumCTC2 > 0)
			{
				for (int i = 0; i < m_NumCTC2; i++)
				{
					if (m_RGNDataList[i]->m_Ratio2 < (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
						m_NumCTC1 = i + 1;
					else
						break;
				}
			}
			start1 = 0;
			if (m_NumCTC1 > 0)
				start1 = 1;
			start2 = 0;
			if (m_NumCTC2 > 0)
			{
				if (start1 == 0)
					start2 = 1;
				else
					start2 = m_NumCTC1 + 1;
			}
			start3 = 0;
			if (((int)m_RGNDataList.size() - m_NumCTC2) > 0)
			{
				if ((start1 == 0) && (start2 == 0))
					start3 = 1;
				else
					start3 = m_NumCTC2 + 1;
			}
		}
		else
		{
			m_NumCTC1 = (int)m_RGNDataList.size();
			m_NumCTC2 = (int)m_RGNDataList.size();
			start1 = 1;
			start2 = 0;
			start3 = 0;
		}
		m_StartingIndices.Format(_T("%d,%d,%d"), start1, start2, start3);
		UpdateData(FALSE);
		WCHAR *char1 = _T("\\");
		int fileStringIndex = m_FullImagePathname.ReverseFind(*char1);
		CString pathname = m_FullImagePathname.Mid(0, fileStringIndex);
		CString regionFileName;
		regionFileName.Format(_T("%s\\%s.rgn"), pathname, m_ImageFilename);
		CFileDialog dlg(FALSE,    // open
			_T(".rgn"),
			regionFileName,    
			OFN_OVERWRITEPROMPT,
			_T("Region Data Files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			CString filename1;
			CString filename = dlg.GetPathName();
			if (filename.Find(_T(".rgn")) > 0)
				filename1 = filename;
			else
				filename1.Format(_T("%s.rgn"), filename);
			fileStringIndex = filename1.ReverseFind(*char1);
			pathname = filename1.Mid(0, fileStringIndex);
			regionFileName = filename1.Mid(fileStringIndex + 1);
			SaveRegionData(pathname, regionFileName);
		}
		EnableButtons(TRUE);
	}
	else
	{
		CString message;
		m_BatchFileName = _T("");
		CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Batch files (*.txt)|*.txt"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			m_BatchFileName = dlg.GetPathName();
		}
		else
		{
			AfxMessageBox(_T("Failed to specify batch file for batch run"));
			message.Format(_T("Failed to specify batch file for batch run"));
			m_Log.Message(message);
			EnableButtons(TRUE);
			return;
		}
		HANDLE thread = ::CreateThread(NULL, 0, BatchRunOperation, this, CREATE_SUSPENDED, NULL);

		if (!thread)
		{
			AfxMessageBox(_T("Fail to create thread for batch run"));
			message.Format(_T("Failed to creat a thread for batch run"));
			m_Log.Message(message);
			EnableButtons(TRUE);
			return;
		}

		::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
		::ResumeThread(thread);
		message.Format(_T("Launched a thread for batch run"));
		m_Log.Message(message);
		m_Status.Format(_T("Launched a thread for batch run"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

DWORD WINAPI BatchRunOperation(LPVOID param)
{
	CStdioFile theFile;
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	CString message;
	bool success = true;

	if (theFile.Open(ptr->m_BatchFileName, CFile::modeRead | CFile::typeText))
	{
		CString batchFileName = ptr->m_BatchFileName;
		int batchExtIndex = batchFileName.Find(_T(".txt"));
		batchFileName.Format(_T("%s.csv"), batchFileName.Mid(0, batchExtIndex));
		CString textline;
		CStdioFile batchResFile;
		if (batchResFile.Open(batchFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,WBCGreenAverage,WBCGreenRelative,WBCGreenCount,GreenCPI,Count1,Count2,Count3,Time(sec),V4Rev51ManualCTCCapture(%%),ManualCTCCount,V3Rev91ManualCTCCapture(%%),V4Rev51Group1Caught,V4Rev51Group2Cought,V4Rev51Group3Caught,V3Group1Caught,V3Rev91Group2Caught,V3Rev91Group3Caught\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}

		CStdioFile CTCDataFile;
		if (CTCDataFile.Open(_T(".\\CTCDataFile.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,V4Rev51AutoRgnIdx,ManualRgnIdx,V3Rev91AutoRgnIdx,RedRelative,GreenRelative,CellRegionPixelCount,GreenPixelCount\n"));
			CTCDataFile.WriteString(textline);
			CTCDataFile.Close();
		}

		int lineIdx = 0;
		while (theFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("%s Line#%d Error: Fail to read pathname"), ptr->m_BatchFileName, lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("%s Line#%d Error: Fail to read pathname"), ptr->m_BatchFileName, lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString pathname = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("%s Line#%d Error: Fail to read image filename"), ptr->m_BatchFileName, lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("%s Line#%d Error: Fail to read image filename"), ptr->m_BatchFileName, lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString imagefilename = textline.Mid(0, index);
			bool exportTrainingData = false;
			CString regionFileName = _T("");
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index > -1)
			{
				exportTrainingData = true;
				regionFileName = textline.Mid(0, index);
				message.Format(_T("exportTrainingData=%d, regionFileName=%s"), exportTrainingData, regionFileName);
				ptr->m_Log.Message(message);
			}
			WCHAR *ch1 = _T("\\");
			index = pathname.ReverseFind(*ch1);
			CString sampleName = _T("");
			if (index > -1)
			{
				sampleName = pathname.Mid(index + 1);
			}
			else
			{
				ptr->m_Status.Format(_T("CellRegionFinderBatch.txt Line#%d Error: Fail to get samplename"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CellRegionFinderBatch.txt Line#%d Error: Fail to get samplename"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString sampleName1 = _T("");
			int GreenAverage = 0;
			int GreenRelative = 0;
			int GreenCount = 0;
			int GreenCPI = 0;
			int Count1 = 0;
			int Count2 = 0;
			int Count3 = 0;
			int manualCTCCount = 0;
			float timeElapsed = 0.0F;
			float ctcCaptureV4Rev51 = 0.0F;
			float ctcCaptureV3Rev91 = 0.0F;
			int V4Rev51Group1Caught = 0;
			int V4Rev51Group2Caught = 0;
			int V4Rev51Group3Caught = 0;
			int V3Rev91Group1Caught = 0;
			int V3Rev91Group2Caught = 0;
			int V3Rev91Group3Caught = 0;
			if (lineIdx == 1)
			{
				ptr->m_Status.Format(_T("Started %d)%s"), lineIdx, sampleName);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("Started %d)%s"), lineIdx, sampleName);
				ptr->m_Log.Message(message);
			}
			bool ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &GreenAverage, &GreenRelative, &GreenCount, &GreenCPI, &Count1, &Count2, &Count3, &timeElapsed, &sampleName1, 
								exportTrainingData, pathname + _T("\\") + regionFileName, &ctcCaptureV4Rev51, &manualCTCCount, &ctcCaptureV3Rev91, &V4Rev51Group1Caught,
								&V4Rev51Group2Caught, &V4Rev51Group3Caught, &V3Rev91Group1Caught, &V3Rev91Group2Caught, &V3Rev91Group3Caught);
			if (!ret)
			{
				CString filename1;
				index = imagefilename.Find(ptr->Leica_Red_Prefix);
				if (index > -1)
				{
					filename1.Format(_T("%s%s"), ptr->Leica_Red_Prefix, imagefilename.Mid(index + ptr->Leica_Red_Prefix.GetLength() + 1));
					ret = ptr->ReadFilesAndRun(pathname + _T("\\") + filename1, &GreenAverage, &GreenRelative, &GreenCount, &GreenCPI, &Count1, &Count2, &Count3, &timeElapsed, &sampleName1, 
						exportTrainingData, pathname + _T("\\") + regionFileName, &ctcCaptureV4Rev51, &manualCTCCount, &ctcCaptureV3Rev91, &V4Rev51Group1Caught,
						&V4Rev51Group2Caught, &V4Rev51Group3Caught, &V3Rev91Group1Caught, &V3Rev91Group2Caught, &V3Rev91Group3Caught);
				}
				else
				{
					index = imagefilename.Find(_T("_c2_ORG.tif"));
					if (index > -1)
					{
						ptr->Zeiss_Red_Postfix = _T("_c2_ORG.tif");
						ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &GreenAverage, &GreenRelative, &GreenCount, &GreenCPI, &Count1, &Count2, &Count3, &timeElapsed, &sampleName1,
							exportTrainingData, pathname + _T("\\") + regionFileName, &ctcCaptureV4Rev51, &manualCTCCount, &ctcCaptureV3Rev91, &V4Rev51Group1Caught,
							&V4Rev51Group2Caught, &V4Rev51Group3Caught, &V3Rev91Group1Caught, &V3Rev91Group2Caught, &V3Rev91Group3Caught);
						ptr->Zeiss_Red_Postfix = _T("_c3_ORG.tif");
					}
					else
					{
						index = imagefilename.Find(_T("_c3_ORG.tif"));
						if (index > -1)
						{
							ptr->Zeiss_Red_Postfix = _T("_c3_ORG.tif");
							ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &GreenAverage, &GreenRelative, &GreenCount, &GreenCPI, &Count1, &Count2, &Count3, &timeElapsed, &sampleName1,
								exportTrainingData, pathname + _T("\\") + regionFileName, &ctcCaptureV4Rev51, &manualCTCCount, &ctcCaptureV3Rev91, &V4Rev51Group1Caught,
								&V4Rev51Group2Caught, &V4Rev51Group3Caught, &V3Rev91Group1Caught, &V3Rev91Group2Caught, &V3Rev91Group3Caught);
							ptr->Zeiss_Red_Postfix = _T("_c2_ORG.tif");
						}
					}
				}
			}
			if (ret)
			{
				if (!exportTrainingData)
					message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d,%.2f\n"), sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, Count1, Count2, Count3, timeElapsed);
				else
					message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d\n"), sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, Count1, Count2, Count3, 
						timeElapsed, ctcCaptureV4Rev51, manualCTCCount, ctcCaptureV3Rev91, V4Rev51Group1Caught, V4Rev51Group2Caught, V4Rev51Group3Caught,
						V3Rev91Group1Caught, V3Rev91Group2Caught, V3Rev91Group3Caught);
				if (batchResFile.Open(batchFileName, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					batchResFile.SeekToEnd();
					batchResFile.WriteString(message);
					batchResFile.Close();
				}
				if (!exportTrainingData)
					message.Format(_T("%d)%s,%d,%d,%d,%d,%d,%d,%d,%.2f(sec)"), lineIdx, sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, Count1, Count2, Count3, timeElapsed);
				else
					message.Format(_T("%d)%s,%d,%d,%d,%d,%d,%d,%d,%.2f(sec),%.2f%%,%d,%.2f%%,%d,%d,%d,%d,%d,%d"), lineIdx, sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, 
					Count1, Count2, Count3, timeElapsed, ctcCaptureV4Rev51, manualCTCCount, ctcCaptureV3Rev91, V4Rev51Group1Caught, V4Rev51Group2Caught, V4Rev51Group3Caught,
					V3Rev91Group1Caught, V3Rev91Group2Caught, V3Rev91Group3Caught);
				ptr->m_Log.Message(message);
				if (!exportTrainingData)
					ptr->m_Status.Format(_T("%d)%s,%d,%d,%d,%d,%d,%d,%d,%.2f(sec)"), lineIdx, sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, Count1, Count2, Count3, timeElapsed);
				else
					ptr->m_Status.Format(_T("%d)%s,%d,%d,%d,%d,%d,%d,%d,%.2f(sec),%.2f%%,%d,%.2f%%,%d,%d,%d,%d,%d,%d"), lineIdx, sampleName1, GreenAverage, GreenRelative, GreenCount, GreenCPI, 
					Count1, Count2, Count3, timeElapsed, ctcCaptureV4Rev51, manualCTCCount, ctcCaptureV3Rev91, V4Rev51Group1Caught, V4Rev51Group2Caught, V4Rev51Group3Caught,
					V3Rev91Group1Caught, V3Rev91Group2Caught, V3Rev91Group3Caught);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				success = false;
				message.Format(_T("%d) failed"), lineIdx);
				ptr->m_Log.Message(message);
				ptr->m_Status.Format(_T("%d) failed"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				//break;
			}
		}
		theFile.Close();
	}

	if (success)
		ptr->m_Status.Format(_T("Batch Run Completed successfully"));
	else
		ptr->m_Status.Format(_T("Batch Run Failed, please check log file for the error happened"));
	::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	ptr->EnableButtons(TRUE);
	return 0;
}

bool CWBCFinderDlg::ReadFilesAndRun(CString imagefile, int *greenAverage, int *greenRelative, int *greenCount, int *greenCPI, 
	int *count1, int *count2, int *count3, float *elapsedTime, CString *sampleName1, bool exportTrainingData, CString ctcRegionFileName, float *v4CtcCapture, int *manualCTCCount,
	float *v3CtcCapture, int *v4Group1Caught, int *v4Group2Caught, int *v4Group3Caught, int *v3Group1Caught, int *v3Group2Caught, int *v3Group3Caught)
{
	bool ret = false;
	CString message;
	bool loaded = false;
	clock_t time1 = clock();
	CString safeFilename = _T("");
	CString pathname = _T("");

	while (true)
	{
		int index = imagefile.Find(Leica_Red_Prefix, 0);
		if (index == -1)
			index = imagefile.Find(Zeiss_Red_Postfix, 0);
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), 
				imagefile, Leica_Red_Prefix, Zeiss_Red_Postfix);
			m_Log.Message(message);
			break;
		}
		ret = m_RedTIFFData->LoadRawTIFFFile(imagefile);
		if (!ret)
		{
			message.Format(_T("Failed to load %s"), imagefile);
			m_Log.Message(message);
			break;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), imagefile, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = imagefile.Find(Leica_Red_Prefix, 0);
			if (index == -1)
			{
				WCHAR *ch1 = _T("\\");
				index = imagefile.ReverseFind(*ch1);
				pathname = imagefile.Mid(0, index);
				safeFilename = imagefile.Mid(index + 1);
				int postfixIndex = imagefile.Find(Zeiss_Red_Postfix);
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), 
						imagefile, Leica_Red_Prefix, Zeiss_Red_Postfix);
					m_Log.Message(message);
					break;
				}
				else
				{
					index = safeFilename.Find(_T(".tif"));
					safeFilename = safeFilename.Mid(0, index);
					CString samplePathName = imagefile.Mid(0, postfixIndex);
					CString filename2 = samplePathName + Zeiss_Green_Postfix;
					ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename2 = samplePathName + Zeiss_Blue_Postfix;
						ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							m_ZeissData = true;
							message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
							m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
							loaded = true;
							*sampleName1 = safeFilename;
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							m_Log.Message(message);
							break;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						m_Log.Message(message);
						break;
					}
				}
			}
			else
			{
				WCHAR *ch1 = _T("\\");
				index = imagefile.ReverseFind(*ch1);
				pathname = imagefile.Mid(0, index);
				safeFilename = imagefile.Mid(index + 1);
				index = imagefile.Find(Leica_Red_Prefix, 0);
				CString postfix = safeFilename.Mid(Leica_Red_Prefix.GetLength());
				index = imagefile.Find(Leica_Red_Prefix, 0);
				CString pathname = imagefile.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, Leica_Green_Prefix, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					m_Log.Message(message);
					break;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					filename2.Format(_T("%s%s%s"), pathname, Leica_Blue_Prefix, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						m_Log.Message(message);
						break;
					}
					else
					{
						m_ZeissData = false;
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
						loaded = true;
						index = postfix.Find(_T(".tif"));
						safeFilename = postfix.Mid(0, index);
						*sampleName1 = safeFilename;
					}
				}
			}
		}

		if (loaded)
		{
			FreeRGNList(&m_RGNDataList);
			m_HitFinder.ScanImage(&m_CTCParams, m_RedTIFFData, m_GreenTIFFData, m_BlueTIFFData, &m_RGNDataList, true);
			m_TotalHits = (int)m_RGNDataList.size();
			m_HitIndex = 1;
			m_NumCTC1 = 0;
			m_NumCTC2 = 0;
			for (int i = 0; i < m_TotalHits; i++)
			{
				if (m_RGNDataList[i]->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
				{
					double nominator = m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * m_RGNDataList[i]->m_RedValue
						+ m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * m_RGNDataList[i]->m_GreenValue;
					double denominator = m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * m_RGNDataList[i]->m_RedValue
						+ m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * m_RGNDataList[i]->m_GreenValue + 1.0;
					if ((nominator > 0) && (denominator > 0))
						m_RGNDataList[i]->m_Ratio2 = nominator / denominator;
					else if (nominator <= 0)
						m_RGNDataList[i]->m_Ratio2 = 0;
					else
						m_RGNDataList[i]->m_Ratio2 = nominator;
					m_NumCTC2 = i + 1;
				}
				else
					break;
			}
			if (m_NumCTC2 > 0)
			{
				QuickSort(&m_RGNDataList, 0, m_NumCTC2 - 1);
				for (int i = 0; i < m_NumCTC2; i++)
				{
					if (m_RGNDataList[i]->m_Ratio2 < (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
						m_NumCTC1 = i + 1;
					else
						break;
				}
			}
			int start1 = 0;
			if (m_NumCTC1 > 0)
				start1 = 1;
			int start2 = 0;
			if (m_NumCTC2 > 0)
			{
				if (start1 == 0)
					start2 = 1;
				else
					start2 = m_NumCTC1 + 1;
			}
			int start3 = 0;
			if (((int)m_RGNDataList.size() - m_NumCTC2) > 0)
			{
				if ((start1 == 0) && (start2 == 0))
					start3 = 1;
				else
					start3 = m_NumCTC2 + 1;
			}
			m_StartingIndices.Format(_T("%d,%d,%d"), start1, start2, start3);
			GetWBCGreenValues();
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			clock_t time2 = clock();
			clock_t timediff = time2 - time1;
			*elapsedTime = ((float)timediff) / CLOCKS_PER_SEC;
			*greenAverage = m_CTCParams.m_WBCGreenAvg;
			*greenRelative = m_WBCGreenRelative;
			*greenCount = m_WBCGreenCount;
			*greenCPI = m_GreenTIFFData->GetCPI();
			*count1 = m_NumCTC1;
			*count2 = m_NumCTC2 - m_NumCTC1;
			*count3 = m_TotalHits - m_NumCTC2;
			CString regionFileName;
			regionFileName.Format(_T("%s.rgn"), safeFilename);
			SaveRegionData(pathname, regionFileName);
			if (exportTrainingData)
			{
				ExportTrainingData(ctcRegionFileName, *sampleName1, v4CtcCapture, manualCTCCount,
					v3CtcCapture, v4Group1Caught, v4Group2Caught, v4Group3Caught, v3Group1Caught, v3Group2Caught, v3Group3Caught);
			}
		}
		else
		{
			message.Format(_T("loaded=false"));
			m_Log.Message(message);
			ret = false;
		}
		break;
	}
	return ret;
}

void CWBCFinderDlg::ExportTrainingData(CString regionFilename, CString sampleName, float *v4CtcCapture, int *manualCTCCount,
	float *v3CtcCapture, int *v4Group1Caught, int *v4Group2Caught, int *v4Group3Caught, int *v3Group1Caught, int *v3Group2Caught, int *v3Group3Caught)
{
	vector<CRGNData *> manualReviewedRegion;
	vector<CRGNData *> v3Group1Region;
	vector<CRGNData *> v3Group2Region;
	vector<CRGNData *> v3Group3Region;
	int ctcCount = 0;
	int ctcMatchCount = 0;
	int group1Caught = 0;
	int group2Caught = 0;
	int group3Caught = 0;
	int v3ctcMatchCount = 0;
	int v3group1Caught = 0;
	int v3group2Caught = 0;
	int v3group3Caught = 0;
	if (LoadRegionData(regionFilename, &manualReviewedRegion))
	{
		WCHAR *ch1 = _T("\\");
		int index = regionFilename.ReverseFind(*ch1);
		CString v3regionfilepathname = regionFilename.Mid(0, index);
		CString v3regionfilename;
		v3regionfilename.Format(_T("%s\\%s_1_V3Rev91.rgn"), v3regionfilepathname, sampleName);
		LoadRegionData(v3regionfilename, &v3Group1Region);
		v3regionfilename.Format(_T("%s\\%s_2_V3Rev91.rgn"), v3regionfilepathname, sampleName);
		LoadRegionData(v3regionfilename, &v3Group2Region);
		v3regionfilename.Format(_T("%s\\%s_3_V3Rev91.rgn"), v3regionfilepathname, sampleName);
		LoadRegionData(v3regionfilename, &v3Group3Region);
		for (int i = 0; i < (int)manualReviewedRegion.size(); i++)
		{
			unsigned int color = manualReviewedRegion[i]->GetColorCode();
			if ((color == CTC) || (color == CTC2))
			{
				bool v4Caught = false;
				bool v3Caught = false;
				ctcCount++;
				int x0, y0;
				manualReviewedRegion[i]->GetPosition(&x0, &y0);
				int minDist = MAXINT;
				int minIndex = -1;
				for (int j = 0; j < (int)m_RGNDataList.size(); j++)
				{
					if (m_RGNDataList[j] == NULL)
						continue;
					int x1, y1;
					m_RGNDataList[j]->GetPosition(&x1, &y1);
					int dx = abs(x1 - x0);
					int dy = abs(y1 - y0);
					int distance = (int)sqrt((double)(dx * dx) + (double)(dy * dy));
					if (distance < minDist)
					{
						minDist = distance;
						minIndex = j;
					}
				}
				
				if ((minIndex > -1) && (minDist < 35))
				{
					ctcMatchCount++;
					v4Caught = true;
					if (minIndex < m_NumCTC1)
						group1Caught++;
					else if (minIndex < m_NumCTC2)
						group2Caught++;
					else
						group3Caught++;
				}

				int v3minDist = MAXINT;
				int v3minIndex = -1;
				for (int j = 0; j < (int)v3Group1Region.size(); j++)
				{
					if (v3Group1Region[j] == NULL)
						continue;
					int x1, y1;
					v3Group1Region[j]->GetPosition(&x1, &y1);
					int dx = abs(x1 - x0);
					int dy = abs(y1 - y0);
					int distance = (int)sqrt((double)(dx * dx) + (double)(dy * dy));
					if (distance < v3minDist)
					{
						v3minDist = distance;
						v3minIndex = j;
					}
				}
				for (int j = 0; j < (int)v3Group2Region.size(); j++)
				{
					if (v3Group2Region[j] == NULL)
						continue;
					int x1, y1;
					v3Group2Region[j]->GetPosition(&x1, &y1);
					int dx = abs(x1 - x0);
					int dy = abs(y1 - y0);
					int distance = (int)sqrt((double)(dx * dx) + (double)(dy * dy));
					if (distance < v3minDist)
					{
						v3minDist = distance;
						v3minIndex = j + (int)v3Group1Region.size();
					}
				}
				for (int j = 0; j < (int)v3Group3Region.size(); j++)
				{
					if (v3Group3Region[j] == NULL)
						continue;
					int x1, y1;
					v3Group3Region[j]->GetPosition(&x1, &y1);
					int dx = abs(x1 - x0);
					int dy = abs(y1 - y0);
					int distance = (int)sqrt((double)(dx * dx) + (double)(dy * dy));
					if (distance < v3minDist)
					{
						v3minDist = distance;
						v3minIndex = j + (int)v3Group1Region.size() + (int)v3Group2Region.size();
					}
				}

				if ((v3minIndex > -1) && (v3minDist < 35))
				{
					v3ctcMatchCount++;
					v3Caught = true;
					if (v3minIndex < (int)v3Group1Region.size())
						v3group1Caught++;
					else if (v3minIndex < ((int)v3Group1Region.size() + (int)v3Group2Region.size()))
						v3group2Caught++;
					else
						v3group3Caught++;
				}

				CStdioFile CTCDataFile;
				if (CTCDataFile.Open(_T(".\\CTCDataFile.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					CTCDataFile.SeekToEnd();
					CString message;
					if (v4Caught)
					{
						CRGNData *rgnPtr = m_RGNDataList[minIndex];
						int pixels = rgnPtr->GetPixels(RB_COLORS);
						if (rgnPtr->GetPixels(GB_COLORS) > pixels)
							pixels = rgnPtr->GetPixels(GB_COLORS);
						message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d\n"), sampleName, minIndex + 1, i + 1, v3minIndex + 1, rgnPtr->m_RedValue, rgnPtr->m_GreenValue, 
							pixels, rgnPtr->GetPixels(GREEN_COLOR));
					}
					else
					{
						if (v3Caught)
							message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d\n"), sampleName, -1, i + 1, v3minIndex + 1, 0, 0, 0, 0);
						else
							message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d\n"), sampleName, -1, i + 1, -1, 0, 0, 0, 0);
					}
					CTCDataFile.WriteString(message);
					CTCDataFile.Close();
				}
				delete m_RGNDataList[minIndex];
				m_RGNDataList[minIndex] = NULL;
				if (v3minIndex < (int)v3Group1Region.size())
				{
					delete v3Group1Region[v3minIndex];
					v3Group1Region[v3minIndex] = NULL;
				}
				else if (v3minIndex < ((int)v3Group1Region.size() + (int)v3Group2Region.size()))
				{
					delete v3Group2Region[v3minIndex - (int)v3Group1Region.size()];
					v3Group2Region[v3minIndex - (int)v3Group1Region.size()] = NULL;
				}
				else
				{
					delete v3Group3Region[v3minIndex - (int)v3Group1Region.size() - (int)v3Group2Region.size()];
					v3Group3Region[v3minIndex - (int)v3Group1Region.size() - (int)v3Group2Region.size()] = NULL;
				}
			}
		}
	}
	FreeRGNList(&manualReviewedRegion);
	FreeRGNList(&v3Group1Region);
	FreeRGNList(&v3Group1Region);
	FreeRGNList(&v3Group1Region);
	if (ctcCount > 0)
	{
		*v4CtcCapture = (float)(100.0 * ctcMatchCount / ctcCount);
		*v3CtcCapture = (float)(100.0 * v3ctcMatchCount / ctcCount);
		*manualCTCCount = ctcCount;
		*v4Group1Caught = group1Caught;
		*v4Group2Caught = group2Caught;
		*v4Group3Caught = group3Caught;
		*v3Group1Caught = v3group1Caught;
		*v3Group2Caught = v3group2Caught; 
		*v3Group3Caught = v3group3Caught;
	}
}

void CWBCFinderDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	int xLoc = -1;
	int xInterval = (IMAGE_WINDOW_X1 - IMAGE_WINDOW_X0) / NUM_IMAGE_COLUMNS;
	for (int i = 0; i < NUM_IMAGE_COLUMNS; i++)
	{
		if ((point.x >= (IMAGE_WINDOW_X0 + i * xInterval)) &&
			(point.x < (IMAGE_WINDOW_X0 + (i + 1) * xInterval)))
		{
			xLoc = i;
			break;
		}
	}
	int yLoc = -1;
	int yInterval = (IMAGE_WINDOW_Y1 - IMAGE_WINDOW_Y0) / NUM_IMAGE_ROWS;
	for (int i = 0; i < NUM_IMAGE_ROWS; i++)
	{
		if ((point.y >= (IMAGE_WINDOW_Y0 + i * yInterval)) &&
			(point.y < (IMAGE_WINDOW_Y0 + (i + 1) * yInterval)))
		{
			yLoc = i;
			break;
		}
	}
	if ((xLoc > -1) && (yLoc > -1))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int ImageLocationIndex = NUM_IMAGE_COLUMNS * yLoc + xLoc;
		if ((startIndex + ImageLocationIndex) < m_TotalHits)
		{
			vector<CRGNData *> list;
			delete m_RGNDataList[startIndex + ImageLocationIndex];
			m_RGNDataList[startIndex + ImageLocationIndex] = NULL;
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				if (m_RGNDataList[i] != NULL)
				{
					list.push_back(m_RGNDataList[i]);
					m_RGNDataList[i] = NULL;
				}
			}
			m_RGNDataList.clear();
			for (int i = 0; i < (int)list.size(); i++)
			{
				m_RGNDataList.push_back(list[i]);
				list[i] = NULL;
			}
			list.clear();
			m_TotalHits = (int)m_RGNDataList.size();
			if (m_TotalHits > 0)
			{ 
				if (m_HitIndex > m_TotalHits)
				{
					m_HitIndex -= NUM_IMAGES;
					if (m_HitIndex < 1)
						m_HitIndex = 1;
				}
			}
			else
			{
				m_HitIndex = 0;
			}
			UpdateData(FALSE);
			if (m_Image != NULL)
				m_Image.Destroy();
			m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
			CopyToRGBImage();
			UpdateImageDisplay();
		}
	}
	CDialogEx::OnLButtonDown(nFlags, point);
}

template<typename T>
void CWBCFinderDlg::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CWBCFinderDlg::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	double pos = (*list)[position]->m_Ratio2;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->m_Ratio2 < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CWBCFinderDlg::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	double pos0 = (*list)[lo]->m_Ratio2;
	double pos1 = (*list)[(lo + hi) / 2]->m_Ratio2;
	double pos2 = (*list)[hi]->m_Ratio2;
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CWBCFinderDlg::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

bool CWBCFinderDlg::ReadRgnfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	CString message;
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("RGN files (*.rgn)|*.rgn"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		if (LoadRegionData(filename, &m_RGNDataList))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%u"), filename, (int)m_RGNDataList.size());
			m_Log.Message(message);
			ret = true;
		}
		else
		{
			m_Status.Format(_T("Failed to load RGN File %s"), filename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	else
	{
		m_Status = _T("Failed to provide RGN File Name");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	return true;
}

BOOL CWBCFinderDlg::LoadRegionData(CString filename, vector<CRGNData *> *regionList)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											if ((color != CTC) && (color != CTC2))
												color = NONCTC;
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														CRGNData* data = new CRGNData(x0, y0, PATCH_WIDTH, PATCH_HEIGHT);
														data->SetColorCode(color);
														data->SetCPI(RED_COLOR, (int)m_RedTIFFData->GetCPI());
														data->SetCPI(GREEN_COLOR, (int)m_GreenTIFFData->GetCPI());
														data->SetCPI(BLUE_COLOR, (int)m_BlueTIFFData->GetCPI());
														data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
														data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
														data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
														data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
														data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
														data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
														data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
														data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
														data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
														regionList->push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	return ret;
}

void CWBCFinderDlg::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}