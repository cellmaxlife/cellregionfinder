
// WBCFinderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"

using namespace std;


// CWBCFinderDlg dialog
class CWBCFinderDlg : public CDialogEx
{
// Construction
public:
	CWBCFinderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WBCFINDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	void PaintImages();
	
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	unsigned short *m_RedPatchImage;
	unsigned short *m_GreenPatchImage;
	unsigned short *m_BluePatchImage;
	CStatic m_Display;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	CString m_ImageFilename;
	int m_HitIndex;
	int m_TotalHits;
	afx_msg void OnBnClickedNexthit();
	afx_msg void OnBnClickedPrevhit();
	afx_msg void OnBnClickedGotohit();
	BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnEnableButtons(WPARAM wparam, LPARAM lparam);
	CHitFindingOperation m_HitFinder;
	vector<CRGNData *> m_RGNDataList;
	void CopyToRGBImage();
	CCTCParams m_CTCParams;
	void UpdateImageDisplay();
	CLog m_Log;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	CButton m_ShowBoundary;
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	afx_msg void OnBnClickedRedcolor();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedShow();
	void SaveRegionFile(CString pathname, CString filename);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void EnableButtons(BOOL enable);
	CString m_FullImagePathname;
	afx_msg void OnBnClickedSavergn();
	void SaveRegionData(CString pathname, CString filename);
	bool m_ZeissData;
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	int m_RedCPI, m_GreenCPI, m_BlueCPI;
	CButton m_ScanFramesOnly;
	CString m_TwoThresholds;
	int m_NumNONCTC;
	int m_NumCTC1;
	int m_NumCTC2;
	CString m_StartingIndices;
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Partition(vector<CRGNData *> *list, int lo, int hi);
	int ChoosePivot(vector<CRGNData *> *list, int lo, int hi);
	int m_WBCGreenRelative;
	int m_WBCGreenCount;
	bool ReadFilesAndRun(CString imagefile, int *greenAverage, int *greenRelative, int *greenCount, int *greenCPI, int *count1, int *count2, int *count3, 
		float *elapsedTime, CString *sampleName1, bool exportTrainingData, CString rgnFilename, float *v4CtcCapture, int *manualCTCCount,
		float *v3CtcCapture, int *v4Group1Caught, int *v4Group2Caught, int *v4Group3Caught, int *v3Group1Caught, int *v3Group2Caught, int *v3Group3Caught);
	void GetWBCGreenValues();
	CButton m_DisplayHit;
	bool ReadRgnfile();
	BOOL LoadRegionData(CString pathname, vector<CRGNData *> *regionList);
	CString m_BatchFileName;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	void ExportTrainingData(CString regionFilename, CString sampleName, float *v4CtcCapture, int *manualCTCCount,
		float *v3CtcCapture, int *v4Group1Caught, int *v4Group2Caught, int *v4Group3Caught, int *v3Group1Caught, int *v3Group2Caught, int *v3Group3Caught);
};
