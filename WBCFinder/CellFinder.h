#pragma once
#include "afxwin.h"
#include "RGNData.h"
#include "RegionDataSet.h"

#define PATTERN_WIDTH 100
#define PATTERN_HEIGHT 100

class CCellFinder
{
protected:
	template<typename T> static void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> static void Swap(vector<T> *list, int index1, int index2);
	static int Partition(vector<int> *list, int lo, int hi);
	static int ChoosePivot(vector<int> *list, int lo, int hi);
	static void SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList);
	static void AddOnePixel(vector<INTENSITY_PIXEL_SET *> *sortedList, unsigned short intensity, int position);
	static void ScanPixelsForBlobs(vector<INTENSITY_PIXEL_SET *> *sortedList, int maxIntensityIndex, vector<CBlobData *> *blobList);
	static void ScanPixelsForOneBlob(vector<int> *pixels, CBlobData *blob, vector<int> *remainingPixels);
	static bool BackwardScan(vector<int> *pixels, int position);
	static bool ForwardScan(vector<int> *pixels, int position);
	static void ScanPixelsForBlobBoundary(vector<int> *pixels, vector<int> *boundary);

public:
	static void GetSortedPixelList(unsigned short *image, vector<CBlobData *> *blobList, vector<INTENSITY_PIXEL_SET *> *sortedList);
};


