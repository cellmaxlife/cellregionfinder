#pragma once

enum CTC_PARAM_TYPE
{
	PARAM_RED_CUTOFF,
	PARAM_GREEN_CUTOFF,
	PARAM_BLUE_CUTOFF,
	PARAM_RED_CONTRAST,
	PARAM_GREEN_CONTRAST,
	PARAM_BLUE_CONTRAST,
	PARAM_RED_THRESHOLD,
	PARAM_GREEN_THRESHOLD,
	PARAM_BLUE_THRESHOLD,
	PARAM_MAX_BLOBPIXELCOUNT,
	PARAM_MIN_BLOBPIXELCOUNT,
	PARAM_BOUNDINGBOX_ASPECTRATIO,
	PARAM_BOUNDINGBOX_PIXELCOUNTRATIO,
	PARAM_REDTHRESHOLD_DROP,
	PARAM_SATURATED_REDCOUNT,
	PARAM_SATURATED_BLUECOUNT,
	PARAM_INSIDEREDBLOB_BLUECOUNT,
	PARAM_WBCGREENMARKUP,
	PARAM_LASTONE,
	PARAM_FLOAT_FIRST,
	PARAM_NOMINATOR_R_COEF1,
	PARAM_NOMINATOR_G_COEF1,
	PARAM_DENOMINATOR_R_COEF1,
	PARAM_DENOMINATOR_G_COEF1,
	PARAM_NOMINATOR_R_COEF2,
	PARAM_NOMINATOR_G_COEF2,
	PARAM_DENOMINATOR_R_COEF2,
	PARAM_DENOMINATOR_G_COEF2,
	PARAM_COEF1_THRESHOLD,
	PARAM_COEF2_THRESHOLD,
	PARAM_FLOAT_LAST
};

class CCTCParams
{
public:
	int m_CTCParams[(((int)PARAM_LASTONE) + 1)];
	const wchar_t *m_CTCParamNames[(((int)PARAM_LASTONE) + 1)];
	int m_WBCGreenAvg;

	CCTCParams();
	void LoadCTCParams(CString filename, CString *thresholds);
	void SaveCTCParams(CString filename);

	float m_GroupParams[((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST) + 1];
	const wchar_t *m_GroupNames[((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST) + 1];
};