#pragma once
#include "ColorType.h"
#include "RGNData.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
#include "Log.h"
#include <vector>
#include "BlobData.h"
#include "MSERDetector.h"

using namespace std;

class CHitFindingOperation
{
protected:
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Partition(vector<int> *list, int lo, int hi);
	int ChoosePivot(vector<int> *list, int lo, int hi);
	int Partition(vector<CRGNData *> *list, int lo, int hi);
	int ChoosePivot(vector<CRGNData *> *list, int lo, int hi);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void GetBlobBoundary(CBlobData *blob);
	CMSERDetector m_MSERDetector;
	bool FindCompositeBlobs(CRGNData *hitData, POINT pos, vector<CBlobData *> *remainingBlobs);
	int GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2);
	void CleanUpExclusionArea(CSingleChannelTIFFData *redImg, CSingleChannelTIFFData *greenImg, CSingleChannelTIFFData *blueImg);
	void SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height);
	void ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection);
	int GetStepEdgePosition(int *profile[], int length, bool startFromBeginning);
	void LowpassFiltering(int *profile, int length);
	bool IsCircularBlob(CBlobData *blob);
	void MergeBlobs(CRGNData *hitData);
	int GetAverageIntensity(unsigned short *image, vector<int> *pixels);
	void GetBlueBlobs(CRGNData *region, vector<POINT> *posList, bool scanMode);
	void ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height);
	void FreeBlobList(vector<CBlobData *> *blobList);
	void GetCompositeBlob(vector<int> *compositeBlob, vector<int> *blobPixels);
	bool blobIncluded(vector<int> *list, int blobIndex);
	void MergeBlobs(CRGNData *hitData, PIXEL_COLOR_TYPE color, int blueBlobIndex, vector<int> *blueBlobIndexList);
	int GetBlueBlobIndex(CRGNData *hitData, POINT pos);
	void CalculateBoundary(CRGNData *hitData);
	void CalculateBoundary(vector<CBlobData *> *blobData);
	void SetFrameMax(CRGNData *region, vector<CRGNData *> *additionRegions);
	int GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color);
	void CalculateCellAttributes(CCTCParams *params, CRGNData *region, bool IsZeissData);
	void SetBoundingBox(CRGNData *region, vector<int> *map);

public:
	CHitFindingOperation();
	virtual ~CHitFindingOperation();
	CLog *m_Log;
	void ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, vector<CRGNData *> *list, bool isBatchMode);
	bool ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData);
	void ScanSingleFrame(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, vector<CRGNData *> *list, int FrameX0, int FrameY0);
	void SortRegionList(vector<CRGNData *> *regionList);
};

