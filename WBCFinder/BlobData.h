#pragma once
#include <vector>
#include "RegionDataSet.h"
#include "ColorType.h"

using namespace std;

class CBlobData
{
public:
	CBlobData(int width, int height);
	virtual ~CBlobData();
	int m_Width;
	int m_Height;
	vector<int> *m_Boundary;
	vector<int> *m_Pixels;
	vector<int> *m_Artifact;
	int m_Intensity;
	int m_IntensityPos;
	int m_Threshold;
};
