#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>

#define MAX_NUCLEUS_PIXELS	500
#define MIN_BLUE_CUTOFF		40
#define MIN_OVERLAPPED_PIXELS	5
#define MAX_POSITION_OFFSET	 5
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

CHitFindingOperation::CHitFindingOperation()
{
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
			delete (*blobList)[i];
	}
	blobList->clear();
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *temp2 = new unsigned char[blob->m_Width * blob->m_Height];
	memset(temp2, 0, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (int)((*blob->m_Pixels)[i] % blob->m_Width);
		int y = (int)((*blob->m_Pixels)[i] / blob->m_Width);
		temp2[blob->m_Width * y + x] = (unsigned char)255;
	}
	ErosionOperation(temp2, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((temp2[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				int pos = (unsigned int) (i * blob->m_Width + j);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
	delete[] temp2;
}

void CHitFindingOperation::ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList, bool isBatchMode)
{
	CString message;
	for (int i = 0; i < (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), params->m_CTCParamNames[i], params->m_CTCParams[i]);
		m_Log->Message(message);
	}
	for (int i = 1; i < ((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST); i++)
	{
		message.Format(_T("%s=%.1f"), params->m_GroupNames[i], params->m_GroupParams[i]);
		m_Log->Message(message);
	}

	CleanUpExclusionArea(redImage, greenImage, blueImage);

	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;

	int NumColumns = redImage->GetImageWidth() / regionWidth;
	int NumRows = redImage->GetImageHeight() / regionHeight;

	message.Format(_T("NumRows=%d,NumColumns=%d"), NumRows, NumColumns);
	m_Log->Message(message);

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	vector<POINT> posList;
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = regionWidth * j;
			int y0 = regionHeight * i;
			bool ret = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (ret)
			{
				CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
				region->SetImages(NULL, NULL, blueRegionImage);
				region->SetCPI(RED_COLOR, redImage->GetCPI());
				region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
				region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
				region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
				region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
				region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
				GetBlueBlobs(region, &posList, true);
				region->NullImages();
				delete region;
			}
		}
	}
	
	message.Format(_T("#BlueBlobs(Cell Region Cadidates)=%u"), posList.size());
	m_Log->Message(message);

	vector<CRGNData *> cellList;
	for (int i = 0; i < (int)posList.size(); i++)
	{
		POINT pos = posList[i];
		int x0 = (int)pos.x - (SAMPLEFRAMEWIDTH / 2);
		int y0 = (int)pos.y - (SAMPLEFRAMEHEIGHT / 2);
		bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
		bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
		bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
		if (retRed && retGreen && retBlue)
		{
			CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
			region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
			region->SetCPI(RED_COLOR, redImage->GetCPI());
			region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
			region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
			region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
			region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
			region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
			region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
			region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
			region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
			region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
			region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
			region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
			m_MSERDetector.GetBlobs(region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR));
			bool found = FindCompositeBlobs(region, pos, NULL);
			if (found)
			{
				bool isRed = true;
				if ((region->GetBlobData(RB_COLORS)->size() > 0) && (region->GetBlobData(GB_COLORS)->size() > 0))
				{
					if ((*region->GetBlobData(RB_COLORS))[0]->m_Pixels->size() >= (*region->GetBlobData(GB_COLORS))[0]->m_Pixels->size())
						isRed = true;
					else
						isRed = false;
				}
				else if (region->GetBlobData(RB_COLORS)->size() > 0)
					isRed = true;
				else if (region->GetBlobData(GB_COLORS)->size() > 0)
					isRed = false;
				else
					found = false;
				if (found)
				{
					CBlobData *blob = NULL;
					if (isRed)
						blob = (*region->GetBlobData(RB_COLORS))[0];
					else
						blob = (*region->GetBlobData(GB_COLORS))[0];
					if (((int)blob->m_Pixels->size() >= params->m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT]) &&
						((int)blob->m_Pixels->size() <= params->m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]) &&
						(IsCircularBlob(blob)))
					{
						CalculateBoundary(region);
						CalculateCellAttributes(params, region, redImage->IsZeissData());
						SetFrameMax(region, NULL);
						region->NullImages();
						cellList.push_back(region);
						region = NULL;
					}
				}
			}
			if (region != NULL)
			{
				region->NullImages();
				delete region;
			}
		}
	}

	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
	posList.clear();

	message.Format(_T("#Cells found before sorting =%u"), cellList.size());
	m_Log->Message(message);

	QuickSort(&cellList, 0, (int)cellList.size() - 1);

	message.Format(_T("Sorting Done"));
	m_Log->Message(message);

	for (int i = 0; i < (int)cellList.size() - 1; i++)
	{
		if (cellList[i] != NULL)
		{
			int xi, yi;
			cellList[i]->GetPosition(&xi, &yi);
			for (int j = i + 1; j < (int)cellList.size(); j++)
			{
				if (cellList[j] != NULL)
				{
					int xj, yj;
					cellList[j]->GetPosition(&xj, &yj);
					if ((abs(xi - xj) < 5) && (abs(yi - yj) < 5))
					{
						delete cellList[j];
						cellList[j] = NULL;
					}
				}
			}
		}
	}

	for (int i = (int)cellList.size() - 1; i >= 0; i--)
	{
		if (cellList[i] != NULL)
		{
			CRGNData *ptr = cellList[i];
			outputList->push_back(ptr);
			cellList[i] = NULL;
		}
	}

	cellList.clear();

	message.Format(_T("done with Coping to outputList, #cells=%u"), outputList->size());
	m_Log->Message(message);
}

void CHitFindingOperation::GetBlueBlobs(CRGNData *region, vector<POINT> *posList, bool scanMode)
{
	m_MSERDetector.GetBlobs(region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR));
	if (region->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		region->GetPosition(&x0, &y0);
		int blueThreshold = region->GetCPI(BLUE_COLOR) + MIN_BLUE_CUTOFF;
		for (int i = 0; i < (int)region->GetBlobData(BLUE_COLOR)->size(); i++)
		{
			CBlobData *blob = (*region->GetBlobData(BLUE_COLOR))[i];
			if ((blob->m_Pixels->size() < MAX_NUCLEUS_PIXELS) &&
				(blob->m_Threshold > blueThreshold))
			{
				POINT pt;
				pt.x = x0 + (blob->m_IntensityPos % blob->m_Width);
				pt.y = y0 + (blob->m_IntensityPos / blob->m_Width);
				posList->push_back(pt);
			}
		}
		if (scanMode)
			FreeBlobList(region->GetBlobData(BLUE_COLOR));
	}
}

int CHitFindingOperation::GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2)
{
	int count = 0;
	int index1 = 0;
	int index2 = 0;
	while ((index1 < (int)blob1->size()) && (index2 < (int)blob2->size()))
	{
		if ((*blob1)[index1] < (*blob2)[index2])
			index1++;
		else if ((*blob1)[index1] > (*blob2)[index2])
			index2++;
		else
		{
			count++;
			index1++;
			index2++;
		}
	}
	return count;
}

void CHitFindingOperation::MergeBlobs(CRGNData *hitData, PIXEL_COLOR_TYPE color, int blueBlobIndex, vector<int> *blueBlobIndexList)
{
	vector<int> connectedToBlue;
	CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[blueBlobIndex];
	vector<int> compositeBlob;
	for (int i = 0; i < (int)blueBlob->m_Pixels->size(); i++)
		compositeBlob.push_back((*blueBlob->m_Pixels)[i]);
	int totalOverlaps = 0;
	bool connected = false;
	for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
	{
		CBlobData *blob = (*hitData->GetBlobData(color))[i];
		int overlappedPixels = GetOverlapPixelCount(blueBlob->m_Pixels, blob->m_Pixels);
		if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
		{
			if (!connected)
			{
				connected = true;
				blueBlobIndexList->push_back(blueBlobIndex);
			}
			totalOverlaps += overlappedPixels;
			GetCompositeBlob(&compositeBlob, blob->m_Pixels);
			connectedToBlue.push_back(i);
		}
	}

	if (connected)
	{
		bool addedNewBlob = true;
		while (addedNewBlob)
		{
			addedNewBlob = false;
			for (int i = 0; i < (int)hitData->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				if (blobIncluded(blueBlobIndexList, i))
					continue;
				CBlobData *blob = (*hitData->GetBlobData(BLUE_COLOR))[i];
				int overlappedPixels = GetOverlapPixelCount(&compositeBlob, blob->m_Pixels);
				if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
				{
					blueBlobIndexList->push_back(i);				
					totalOverlaps += overlappedPixels;
					GetCompositeBlob(&compositeBlob, blob->m_Pixels);
					addedNewBlob = true;
				}
			}
			if (addedNewBlob)
			{
				addedNewBlob = false;
				for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
				{
					if (blobIncluded(&connectedToBlue, i))
						continue;
					CBlobData *blob = (*hitData->GetBlobData(color))[i];
					int overlappedPixels = GetOverlapPixelCount(&compositeBlob, blob->m_Pixels);
					if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
					{
						connectedToBlue.push_back(i);
						totalOverlaps += overlappedPixels;
						GetCompositeBlob(&compositeBlob, blob->m_Pixels);
						addedNewBlob = true;
					}
				}
			}
		}
		CBlobData *blob = new CBlobData(SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
		if (color == RED_COLOR)
		{
			hitData->m_RedBlueOverlaps = totalOverlaps;
			hitData->GetBlobData(RB_COLORS)->push_back(blob);
		}
		else
		{
			hitData->m_GreenBlueOverlaps = totalOverlaps;
			hitData->GetBlobData(GB_COLORS)->push_back(blob);
		}
		for (int i = 0; i < (int)compositeBlob.size(); i++)
			blob->m_Pixels->push_back(compositeBlob[i]);
		compositeBlob.clear();
		vector<CBlobData *> list;
		for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
		{
			blob = (*hitData->GetBlobData(color))[i];
			(*hitData->GetBlobData(color))[i] = NULL;
			if (blobIncluded(&connectedToBlue, i))
				list.push_back(blob);
			else
				delete blob;
		}
		hitData->GetBlobData(color)->clear();
		for (int i = 0; i < (int)list.size(); i++)
		{
			hitData->GetBlobData(color)->push_back(list[i]);
			list[i] = NULL;
		}
		list.clear();
	}
}

void CHitFindingOperation::GetCompositeBlob(vector<int> *compositeBlob, vector<int> *blobPixels)
{
	vector<int> pixels;
	for (int i = 0; i < (int)compositeBlob->size(); i++)
		pixels.push_back((*compositeBlob)[i]);
	for (int i = 0; i < (int)blobPixels->size(); i++)
		pixels.push_back((*blobPixels)[i]);
	QuickSort(&pixels, 0, ((int)pixels.size() - 1));
	int index = 1;
	int lastPos = pixels[0];
	compositeBlob->clear();
	compositeBlob->push_back(lastPos);
	while (index < (int)pixels.size())
	{
		if (pixels[index] == lastPos)
			index++;
		else
		{
			lastPos = pixels[index];
			compositeBlob->push_back(lastPos);
			index++;
		}
	}
	pixels.clear();
}

bool CHitFindingOperation::blobIncluded(vector<int> *list, int blobIndex)
{
	bool ret = false;
	for (int i = 0; i < (int)list->size(); i++)
	{
		if (blobIndex == (*list)[i])
		{
			ret = true;
			break;
		}
	}

	return ret;
}

int CHitFindingOperation::GetBlueBlobIndex(CRGNData *hitData, POINT pos)
{
	int index = -1;
	if ((int)hitData->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		hitData->GetPosition(&x0, &y0);
		int x1 = (int)pos.x - x0;
		int y1 = (int)pos.y - y0;
		int difference = MAXINT;
		int minIndex = -1;
		for (int k = 0; k < (int)hitData->GetBlobData(BLUE_COLOR)->size(); k++)
		{
			CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[k];
			int x2 = blueBlob->m_IntensityPos % blueBlob->m_Width;
			int y2 = blueBlob->m_IntensityPos / blueBlob->m_Width;
			if ((abs(x2 - x1) + abs(y2 - y1)) < difference)
			{
				difference = abs(x2 - x1) + abs(y2 - y1);
				minIndex = k;
			}
		}
		if (difference < MAX_POSITION_OFFSET)
			index = minIndex;
	}
	return index;
}

bool CHitFindingOperation::FindCompositeBlobs(CRGNData *hitData, POINT pos, vector<CBlobData *> *remainingBlobs)
{
	bool found = false;
	int blueBlobIndex = GetBlueBlobIndex(hitData, pos);
	if (blueBlobIndex > -1)
	{
		hitData->m_RedBlueOverlaps = 0;
		hitData->m_GreenBlueOverlaps = 0;
		vector<int> blueInRB;
		m_MSERDetector.GetBlobs(hitData->GetImage(RED_COLOR), hitData->GetBlobData(RED_COLOR));
		MergeBlobs(hitData, RED_COLOR, blueBlobIndex, &blueInRB);
		vector<int> blueInGB;
		m_MSERDetector.GetBlobs(hitData->GetImage(GREEN_COLOR), hitData->GetBlobData(GREEN_COLOR));
		MergeBlobs(hitData, GREEN_COLOR, blueBlobIndex, &blueInGB);
		if ((blueInRB.size() > 0) || (blueInGB.size() > 0))
			found = true;
		if (found)
		{
			vector<CBlobData *> list;
			for (int i = 0; i < (int)hitData->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				CBlobData *blob = (*hitData->GetBlobData(BLUE_COLOR))[i];
				(*hitData->GetBlobData(BLUE_COLOR))[i] = NULL;
				if ((blobIncluded(&blueInRB, i)) || (blobIncluded(&blueInGB, i)))
					list.push_back(blob);
				else
				{
					if (remainingBlobs == NULL)
						delete blob;
					else
						remainingBlobs->push_back(blob);
				}
			}
			hitData->GetBlobData(BLUE_COLOR)->clear();
			for (int i = 0; i < (int)list.size(); i++)
			{
				hitData->GetBlobData(BLUE_COLOR)->push_back(list[i]);
				list[i] = NULL;
			}
			list.clear();
		}
	}
	return found;
}
template<typename T>
void CHitFindingOperation::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CHitFindingOperation::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

int CHitFindingOperation::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	double pos = (*list)[position]->m_Ratio1;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->m_Ratio1 < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	double pos0 = (*list)[lo]->m_Ratio1;
	double pos1 = (*list)[(lo + hi) / 2]->m_Ratio1;
	double pos2 = (*list)[hi]->m_Ratio1;
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CHitFindingOperation::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CHitFindingOperation::CleanUpExclusionArea(CSingleChannelTIFFData *redImg, CSingleChannelTIFFData *greenImg, CSingleChannelTIFFData *blueImg)
{
	const int ARRAYSIZE = 3000;

	int width = redImg->GetImageWidth();
	int height = redImg->GetImageHeight();

	RECT rect[12]; // 0: Left, 1: Right, 2:Top, 3:Bottom
	int width1;
	int height1;

	rect[0].left = 0;
	rect[0].right = ARRAYSIZE;
	rect[0].top = height / 2 - 25;
	rect[0].bottom = height / 2 + 25;

	rect[1].left = 0;
	rect[1].right = ARRAYSIZE;
	rect[1].top = height / 2 - 150;
	rect[1].bottom = height / 2 - 100;

	rect[2].left = 0;
	rect[2].right = ARRAYSIZE;
	rect[2].top = height / 2 + 100;
	rect[2].bottom = height / 2 + 150;

	rect[3].left = width - ARRAYSIZE;
	rect[3].right = width;
	rect[3].top = height / 2 - 25;
	rect[3].bottom = height / 2 + 25;

	rect[4].left = width - ARRAYSIZE;
	rect[4].right = width;
	rect[4].top = height / 2 - 150;
	rect[4].bottom = height / 2 - 100;

	rect[5].left = width - ARRAYSIZE;
	rect[5].right = width;
	rect[5].top = height / 2 + 100;
	rect[5].bottom = height / 2 + 150;

	rect[6].left = width / 2 - 25;
	rect[6].right = width / 2 + 25;
	rect[6].top = 0;
	rect[6].bottom = ARRAYSIZE;

	rect[7].left = width / 2 - 150;
	rect[7].right = width / 2 - 100;
	rect[7].top = 0;
	rect[7].bottom = ARRAYSIZE;

	rect[8].left = width / 2 + 100;
	rect[8].right = width / 2 + 150;
	rect[8].top = 0;
	rect[8].bottom = ARRAYSIZE;

	rect[9].left = width / 2 - 25;
	rect[9].right = width / 2 + 25;
	rect[9].top = height - ARRAYSIZE;
	rect[9].bottom = height;

	rect[10].left = width / 2 - 150;
	rect[10].right = width / 2 - 100;
	rect[10].top = height - ARRAYSIZE;
	rect[10].bottom = height;

	rect[11].left = width / 2 + 100;
	rect[11].right = width / 2 + 150;
	rect[11].top = height - ARRAYSIZE;
	rect[11].bottom = height;

	int left0 = 0;
	int right0 = 0;
	int top0 = 0;
	int bottom0 = 0;

	width1 = rect[0].right - rect[0].left;
	height1 = rect[0].bottom - rect[0].top;
	unsigned short *image = new unsigned short[width1 * height1];
	unsigned short *sumImage = new unsigned short[width1 * height1];
	int *profile[3];
	int size = sizeof(int) * ARRAYSIZE;
	for (int i = 0; i < 3; i++)
	{
		profile[i] = new int[ARRAYSIZE];
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i], 0, size);
		ImageProjection(sumImage, profile[i], width1, height1, true);
		LowpassFiltering(profile[i], ARRAYSIZE);
	}
	left0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 3; i < 6; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 3], 0, size);
		ImageProjection(sumImage, profile[i - 3], width1, height1, true);
		LowpassFiltering(profile[i - 3], ARRAYSIZE);
	}

	right0 = rect[3].left + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;
	delete[] image;
	delete[] sumImage;

	width1 = rect[6].right - rect[6].left;
	height1 = rect[6].bottom - rect[6].top;
	image = new unsigned short[width1 * height1];
	sumImage = new unsigned short[width1 * height1];

	for (int i = 6; i < 9; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 6], 0, size);
		ImageProjection(sumImage, profile[i - 6], width1, height1, false);
		LowpassFiltering(profile[i - 6], ARRAYSIZE);
	}

	top0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 9; i < 12; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 9], 0, size);
		ImageProjection(sumImage, profile[i - 9], width1, height1, false);
		LowpassFiltering(profile[i - 9], ARRAYSIZE);
	}

	bottom0 = rect[9].top + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;

	delete[] image;
	delete[] sumImage;
	for (int i = 0; i < 3; i++)
		delete[] profile[i];

	int centerX = (right0 + left0) / 2;
	int centerY = (top0 + bottom0) / 2;
	int radiusX = centerX - left0;
	int radiusY = centerY - top0;
	CString message;
	message.Format(_T("Exclusion: CenterX=%d, CenterY=%d, RadiusX=%d, RadiusY=%d"), centerX, centerY, radiusX, radiusY);
	m_Log->Message(message);
	message.Format(_T("Exclusion: Left0=%d, Top0=%d, Right0=%d, Bottom0=%d"), left0, top0, right0, bottom0);
	m_Log->Message(message);
	redImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	greenImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	blueImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
}

void CHitFindingOperation::SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height)
{
	unsigned short *ptrImage = image;
	unsigned short *ptrSum = sumImage;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptrImage++, ptrSum++)
		{
			*ptrSum += *ptrImage;
		}
	}
}

void CHitFindingOperation::ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection)
{
	if (verticalProjection)
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				profile[i] += *(sumImage + width * j + i);
			}
		}
	}
	else
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				profile[i] += *(sumImage + width * i + j);
			}
		}
	}
}

int CHitFindingOperation::GetStepEdgePosition(int *profile[], int length, bool startFromBeginning)
{
	int stepsize = 10;
	int length1 = length / stepsize;
	int *temp = new int[length1];
	memset(temp, 0, sizeof(int)* length1);
	if (startFromBeginning)
	{
		for (int i = 3; i < (length - 6); i++)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i + 1) - *(profile[j] + i));
			}
		}
	}
	else
	{
		for (int i = (length - 6); i > 3; i--)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i - 1) - *(profile[j] + i));
			}
		}
	}
	int max = 0;
	int maxi = 0;
	for (int i = 0; i < length1; i++)
	{
		if (temp[i] > max)
		{
			max = temp[i];
			maxi = i;
		}
	}
	delete[] temp;
	return (stepsize * maxi);
}

void CHitFindingOperation::LowpassFiltering(int *profile, int length)
{
	int *temp = new int[length - 8];
	temp[0] = 0;

	for (int i = 0; i < 7; i++)
		temp[0] += profile[i];

	for (int i = 1; i < (length - 8); i++)
		temp[i] = temp[i - 1] - profile[i] + profile[i + 7];

	for (int i = 0; i < 3; i++)
		profile[i] = 0;

	for (int i = 3; i < (length - 5); i++)
		profile[i] = temp[i - 3] / 7;

	for (int i = (length - 5); i < length; i++)
		profile[i] = 0;

	delete temp;
}

bool CHitFindingOperation::IsCircularBlob(CBlobData *blob)
{
	bool ret = false;
	
	int left = blob->m_Width;
	int right = 0;
	int top = blob->m_Height;
	int bottom = 0;
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (*blob->m_Pixels)[i] % blob->m_Width;
		int y = (*blob->m_Pixels)[i] / blob->m_Width;
		if (x < left)
			left = x;
		if (x > right)
			right = x;
		if (y < top)
			top = y;
		if (y > bottom)
			bottom = y;
	}
	int width1 = (right - left + 1);
	int height1 = (bottom - top + 1);
	int longSide = width1;
	int shortSide = height1;
	if (height1 > longSide)
	{
		longSide = height1;
		shortSide = width1;
	}
	int aspectRatio = 100 * shortSide / longSide;
	int extent = 100 * (int)blob->m_Pixels->size() / longSide / shortSide;
	if ((aspectRatio > 35) && (extent > 50))
		ret = true;

	return ret;
}

int CHitFindingOperation::GetAverageIntensity(unsigned short *image, vector<int> *pixels)
{
	int intensity = 0;

	for (int i = 0; i < (int)pixels->size(); i++)
		intensity += image[(*pixels)[i]];

	if ((int)pixels->size() > 0)
		intensity /= (int)pixels->size();

	return intensity;
}

void CHitFindingOperation::CalculateBoundary(CRGNData *hitData)
{
	CalculateBoundary(hitData->GetBlobData(RED_COLOR));
	CalculateBoundary(hitData->GetBlobData(GREEN_COLOR));
	CalculateBoundary(hitData->GetBlobData(BLUE_COLOR));
	CalculateBoundary(hitData->GetBlobData(RB_COLORS));
	CalculateBoundary(hitData->GetBlobData(GB_COLORS));
}

void CHitFindingOperation::CalculateBoundary(vector<CBlobData *> *blobData)
{
	for (int i = 0; i < (int)blobData->size(); i++)
	{
		GetBlobBoundary((*blobData)[i]);
	}
}

bool CHitFindingOperation::ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData)
{
	bool ret = false;
	FreeBlobList(region->GetBlobData(RED_COLOR));
	FreeBlobList(region->GetBlobData(GREEN_COLOR));
	FreeBlobList(region->GetBlobData(BLUE_COLOR));
	FreeBlobList(region->GetBlobData(RB_COLORS));
	FreeBlobList(region->GetBlobData(GB_COLORS));
	region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
	region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
	region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
	vector<POINT> pts;
	GetBlueBlobs(region, &pts, false);
	bool firstRegion = true;
	vector<CBlobData *> list;
	vector<CRGNData *> cells;
	CRGNData *newRegion = NULL;
	for (int i = 0; i < (int)pts.size(); i++)
	{ 
		if (firstRegion)
		{
			bool found = FindCompositeBlobs(region, pts[i], &list);
			if (found)
			{
				ret = true;
				CalculateBoundary(region);
				CalculateCellAttributes(params, region, isZeissData);
				firstRegion = false;
				if ((int)list.size() > 0)
				{
					newRegion = new CRGNData(region);
					for (int j = 0; j < (int)list.size(); j++)
					{
						newRegion->GetBlobData(BLUE_COLOR)->push_back(list[j]);
						list[j] = NULL;
					}
					list.clear();
				}
				else
					break;
			}
		}
		else
		{
			bool found = FindCompositeBlobs(newRegion, pts[i], &list);
			if (found)
			{
				CalculateBoundary(newRegion);
				CalculateCellAttributes(params, newRegion, isZeissData);
				cells.push_back(newRegion);
				newRegion = NULL;
				if ((int)list.size() > 0)
				{
					newRegion = new CRGNData(region);
					for (int j = 0; j < (int)list.size(); j++)
					{
						newRegion->GetBlobData(BLUE_COLOR)->push_back(list[j]);
						list[j] = NULL;
					}
					list.clear();
				}
				else
					break;
			}
		}
	}
	if (newRegion != NULL)
		delete newRegion;
	if (cells.size() > 0)
	{
		int left, right, top, bottom;
		region->GetBoundingBox(&left, &top, &right, &bottom);
		int offsetX = abs(((left + right) / 2) - (region->GetWidth() / 2));
		int offsetY = abs(((top + bottom) / 2) - (region->GetHeight() / 2));
		for (int i = 0; i < (int)cells.size(); i++)
		{
			cells[i]->GetBoundingBox(&left, &top, &right, &bottom);
			int offsetX1 = abs(((left + right) / 2) - (region->GetWidth() / 2));
			int offsetY1 = abs(((top + bottom) / 2) - (region->GetHeight() / 2));
			if ((offsetX1 < offsetX) && (offsetY1 < offsetY))
			{
				offsetX = offsetX1;
				offsetY = offsetY1;
				region->CopyRegionData(cells[i]);
			}
		}
		FreeRGNList(&cells);
	}
	SetFrameMax(region, &cells);
	return ret;
}

void  CHitFindingOperation::SetFrameMax(CRGNData *region, vector<CRGNData *> *additionRegions)
{
	int redMax = 0;
	int greenMax = 0;
	int blueMax = 0;

	for (int i = 0; i < (int)region->GetBlobData(RED_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(RED_COLOR))[i]->m_Intensity > redMax)
			redMax = (*region->GetBlobData(RED_COLOR))[i]->m_Intensity;
	}
	for (int i = 0; i < (int)region->GetBlobData(GREEN_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(GREEN_COLOR))[i]->m_Intensity > greenMax)
			greenMax = (*region->GetBlobData(GREEN_COLOR))[i]->m_Intensity;
	}
	for (int i = 0; i < (int)region->GetBlobData(BLUE_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(BLUE_COLOR))[i]->m_Intensity > blueMax)
			blueMax = (*region->GetBlobData(BLUE_COLOR))[i]->m_Intensity;
	}

	if (additionRegions != NULL)
	{
		for (int j = 0; j < (int)additionRegions->size(); j++)
		{
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(RED_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(RED_COLOR))[i]->m_Intensity > redMax)
					redMax = (*(*additionRegions)[j]->GetBlobData(RED_COLOR))[i]->m_Intensity;
			}
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(GREEN_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(GREEN_COLOR))[i]->m_Intensity > greenMax)
					greenMax = (*(*additionRegions)[j]->GetBlobData(GREEN_COLOR))[i]->m_Intensity;
			}
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(BLUE_COLOR))[i]->m_Intensity > blueMax)
					blueMax = (*(*additionRegions)[j]->GetBlobData(BLUE_COLOR))[i]->m_Intensity;
			}
		}
	}

	region->m_RedFrameMax = redMax;
	region->m_GreenFrameMax = greenMax;
	region->m_BlueFrameMax = blueMax;
	if (additionRegions != NULL)
	{
		for (int j = 0; j < (int)additionRegions->size(); j++)
		{
			(*additionRegions)[j]->m_RedFrameMax = redMax;
			(*additionRegions)[j]->m_GreenFrameMax = greenMax;
			(*additionRegions)[j]->m_BlueFrameMax = blueMax;
		}
	}
}

int CHitFindingOperation::GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int pixelCount = 0;
	
	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
		pixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();
	
	return pixelCount;
}

void CHitFindingOperation::SetBoundingBox(CRGNData *region, vector<int> *map)
{
	int left = region->GetWidth();
	int right = 0;
	int top = region->GetHeight();
	int bottom = 0;

	for (int i = 0; i < (int)map->size(); i++)
	{
		int x = (*map)[i] % region->GetWidth();
		int y = (*map)[i] / region->GetWidth();
		if (x < left)
			left = x;
		if (x > right)
			right = x;
		if (y < top)
			top = y;
		if (y > bottom)
			bottom = y;
	}

	region->SetBoundingBox(left, top, right, bottom);
}

void CHitFindingOperation::CalculateCellAttributes(CCTCParams *params, CRGNData *region, bool IsZeissData)
{
	region->SetPixels(RED_COLOR, GetPixelCountSum(region, RED_COLOR));
	region->SetPixels(GREEN_COLOR, GetPixelCountSum(region, GREEN_COLOR));
	region->SetPixels(BLUE_COLOR, GetPixelCountSum(region, BLUE_COLOR));
	region->SetPixels(RB_COLORS, GetPixelCountSum(region, RB_COLORS));
	region->SetPixels(GB_COLORS, GetPixelCountSum(region, GB_COLORS));
	vector<int> *map;
	if (region->GetPixels(RB_COLORS) >= region->GetPixels(GB_COLORS))
	{
		map = (*region->GetBlobData(RB_COLORS))[0]->m_Pixels;
	}
	else
	{
		map = (*region->GetBlobData(GB_COLORS))[0]->m_Pixels;
	}
	SetBoundingBox(region, map);
	region->m_RedAverage = GetAverageIntensity(region->GetImage(RED_COLOR), map);
	region->m_GreenAverage = GetAverageIntensity(region->GetImage(GREEN_COLOR), map);
	region->m_BlueAverage = GetAverageIntensity(region->GetImage(BLUE_COLOR), map);
	UINT32 *hist = new UINT32[CPI_RANGE_LENGTH];
	memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);
	unsigned short *image = region->GetImage(RED_COLOR);
	for (int i = 0; i < PATCH_HEIGHT; i++)
	{
		for (int j = 0; j < PATCH_WIDTH; j++, image++)
		{
			int value = *image;
			if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
				hist[value - CPI_RANGE_START]++;
		}
	}
	UINT32 maxHistCount = 0;
	int maxHistIndex = 0;
	for (int i = 0; i < CPI_RANGE_LENGTH; i++)
	{
		if (hist[i] > maxHistCount)
		{
			maxHistCount = hist[i];
			maxHistIndex = i;
		}
	}
	region->SetCPI(RED_COLOR, maxHistIndex + CPI_RANGE_START);
	memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);
	image = region->GetImage(GREEN_COLOR);
	for (int i = 0; i < PATCH_HEIGHT; i++)
	{
		for (int j = 0; j < PATCH_WIDTH; j++, image++)
		{
			int value = *image;
			if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
				hist[value - CPI_RANGE_START]++;
		}
	}
	maxHistCount = 0;
	maxHistIndex = 0;
	for (int i = 0; i < CPI_RANGE_LENGTH; i++)
	{
		if (hist[i] > maxHistCount)
		{
			maxHistCount = hist[i];
			maxHistIndex = i;
		}
	}
	region->SetCPI(GREEN_COLOR, maxHistIndex + CPI_RANGE_START);
	delete hist;

	if (region->m_RedAverage > region->GetCPI(RED_COLOR))
		region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
	else
		region->m_RedValue = 0;
	if (region->m_GreenAverage > region->GetCPI(GREEN_COLOR))
		region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
	else
		region->m_GreenValue = 0;

	float nominator = (float) (params->m_GroupParams[(int)PARAM_NOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_NOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue);
	float denominator = (float) (params->m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue + 1.0);

	if ((nominator > 0) && (denominator > 0))
		region->m_Ratio1 = ((double)nominator) / ((double)denominator);
	else if (nominator <= 0)
		region->m_Ratio1 = 0;
	else
	{
		region->m_Ratio1 = nominator;
	}

	if (region->m_Ratio1 <= (double)params->m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
	{
		region->SetColorCode(NONCTC);
	}
	else
	{
		region->SetColorCode(CTC);
	}
}

void CHitFindingOperation::ScanSingleFrame(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList, int FrameX0, int FrameY0)
{
	CString message;

	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;
	
	int NumColumns = CAMERA_FRAME_WIDTH / regionWidth;
	int NumRows = CAMERA_FRAME_HEIGHT / regionHeight;

	message.Format(_T("FrameX0=%d,FrameY0=%d,NumRows=%d,NumColumns=%d"), FrameX0, FrameY0, NumRows, NumColumns);
	m_Log->Message(message);

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	vector<POINT> posList;
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = FrameX0 + regionWidth * j;
			int y0 = FrameY0 + regionHeight * i;
			bool ret = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (ret)
			{
				CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
				region->SetImages(NULL, NULL, blueRegionImage);
				region->SetCPI(RED_COLOR, redImage->GetCPI());
				region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
				region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
				region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
				region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
				region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
				GetBlueBlobs(region, &posList, true);
				region->NullImages();
				delete region;
			}
		}
	}

	message.Format(_T("#BlueBlobs(Cell Region Cadidates)=%u"), posList.size());
	m_Log->Message(message);

	for (int i = 0; i < (int)posList.size(); i++)
	{
		POINT pos = posList[i];
		int x0 = (int)pos.x - (SAMPLEFRAMEWIDTH / 2);
		int y0 = (int)pos.y - (SAMPLEFRAMEHEIGHT / 2);
		bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
		bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
		bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
		if (retRed && retGreen && retBlue)
		{
			CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
			region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
			region->SetCPI(RED_COLOR, redImage->GetCPI());
			region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
			region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
			region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
			region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
			region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
			region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
			region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
			region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
			region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
			region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
			region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
			m_MSERDetector.GetBlobs(region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR));
			bool found = FindCompositeBlobs(region, pos, NULL);
			if (found)
			{
				bool isRed = true;
				int rbSize = 0;
				int gbSize = 0;
				if ((region->GetBlobData(RB_COLORS)->size() > 0) && (region->GetBlobData(GB_COLORS)->size() > 0))
				{
					rbSize = (int)(*region->GetBlobData(RB_COLORS))[0]->m_Pixels->size();
					gbSize = (int)(*region->GetBlobData(GB_COLORS))[0]->m_Pixels->size();
					if (rbSize >= gbSize)
						isRed = true;
					else
						isRed = false;
				}
				else if (region->GetBlobData(RB_COLORS)->size() > 0)
				{
					rbSize = (int)(*region->GetBlobData(RB_COLORS))[0]->m_Pixels->size();
					isRed = true;
				}
				else if (region->GetBlobData(GB_COLORS)->size() > 0)
				{
					gbSize = (int)(*region->GetBlobData(GB_COLORS))[0]->m_Pixels->size();
					isRed = false;
				}
				else
					found = false;
				if (found)
				{
					CBlobData *blob = NULL;
					if (isRed)
						blob = (*region->GetBlobData(RB_COLORS))[0];
					else
						blob = (*region->GetBlobData(GB_COLORS))[0];
					if (((int)blob->m_Pixels->size() >= params->m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT]) &&
						((int)blob->m_Pixels->size() <= params->m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]) &&
						(IsCircularBlob(blob)))
					{
						CalculateBoundary(region);
						CalculateCellAttributes(params, region, redImage->IsZeissData());
						SetFrameMax(region, NULL);
						region->NullImages();
						outputList->push_back(region);
						region = NULL;
					}
				}
			}
			if (region != NULL)
			{
				region->NullImages();
				delete region;
			}
		}
	}

	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
	posList.clear();

	message.Format(_T("#Cells found so far = %u"), outputList->size());
	m_Log->Message(message);
}


void CHitFindingOperation::SortRegionList(vector<CRGNData *> *outputList)
{
	QuickSort(outputList, 0, (int)outputList->size() - 1);

	for (int i = 0; i < (int)(outputList->size() / 2); i++)
	{
		CRGNData *ptr = (*outputList)[i];
		(*outputList)[i] = (*outputList)[(int)outputList->size() - 1 - i];
		(*outputList)[(int)outputList->size() - 1 - i] = ptr;
	}
}